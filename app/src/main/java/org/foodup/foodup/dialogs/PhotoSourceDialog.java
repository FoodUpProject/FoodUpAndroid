package org.foodup.foodup.dialogs;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import org.foodup.foodup.R;
import org.foodup.foodup.utils.Commons;
import org.foodup.foodup.utils.PermissionsManager;

/**
 * Dialog displayed when user wants to upload a picture when configuring users or dishes
 */
public class PhotoSourceDialog extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_photo_source, null);

        builder.setCustomTitle(view);
        builder.setItems(R.array.sources, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        if (!PermissionsManager.hasPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().getBaseContext())) {
                            Commons.askGalleryPermission(getActivity());
                        } else {
                            Commons.pickImageIntent(getActivity());
                        }
                        break;
                    case 1:
                        if (!PermissionsManager.hasPermission(android.Manifest.permission.CAMERA, getActivity()) ||
                                !PermissionsManager.hasPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getActivity().getBaseContext())) {
                            Commons.askCameraPermission(getActivity());
                        } else {
                            Commons.takePictureIntent(getActivity());
                        }
                        break;
                }
            }
        });

        return builder.create();
    }

}