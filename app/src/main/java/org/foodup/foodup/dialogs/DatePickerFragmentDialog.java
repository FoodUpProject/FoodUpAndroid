package org.foodup.foodup.dialogs;

/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TextView;

import org.foodup.foodup.R;

import java.util.Calendar;
import java.util.Date;

/**
 * Dialog displayed when creating FoodUp in order to choose init and end date of menu
 */
public class DatePickerFragmentDialog extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {
    private int date;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //here is your arguments
        Bundle bundle = getArguments();
        //here is your list array
        date = bundle.getInt("requestCode");
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);


        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        switch (date) {
            case 1:
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                break;
            case 2:
                Date initDate = (Date) bundle.getSerializable("initDate");
                if (initDate != null) {
                    datePickerDialog.getDatePicker().setMinDate(initDate.getTime());
                }
                break;
        }
        return datePickerDialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user

        TextView show_date = null;
        switch (date) {
            case 1:
                show_date = (TextView) getActivity().findViewById(R.id.init_date);
                break;
        }

        if (show_date != null) {
            show_date.setText(day + "/" + (month + 1) + "/" + year);
        }
        show_date = (TextView) getActivity().findViewById(R.id.end_date);
        show_date.setText(day + "/" + (month + 1) + "/" + year);
    }
}
