package org.foodup.foodup.dialogs;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.foodup.foodup.R;
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.User;
import org.foodup.foodup.interfaces.UserService;
import org.foodup.foodup.utils.CircleTransform;
import org.foodup.foodup.utils.ServiceGenerator;

import retrofit2.Callback;
import retrofit2.Response;

/**
 * Dialog displayed to add a real user when creating foodUp
 */
public class AddUserDialog extends DialogFragment implements View.OnClickListener {

    private View mView;
    // each data item is just a string in this case
    private ImageView picture;
    private TextView username;
    private User user;
    private EditText user_email;
    private int position;
    private TextView notFound;

    private OnFragmentInteractionListener mListener;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_search_user, null);

        // Inflate and set the layout for the dialog
        // Pass null as the parent card because its going in the dialog layout
        builder.setView(view);

        position = getArguments().getInt("position");
        mView = view.findViewById(R.id.user_view);
        picture = (ImageView) view.findViewById(R.id.user_picture);
        username = (TextView) view.findViewById(R.id.username);
        user_email = (EditText) view.findViewById(R.id.search_user);
        notFound=(TextView)view.findViewById(R.id.not_found);
        ImageButton search_button = (ImageButton) view.findViewById(R.id.search_button);
        search_button.setOnClickListener(this);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!notFound.isShown()){
                mListener.getUser(user, position);
                dismiss();}
            }
        };
        username.setOnClickListener(listener);
        picture.setOnClickListener(listener);
        mView.setOnClickListener(listener);

        return builder.create();
    }

    @Override
    public void onClick(View view) {
        String email = user_email.getText().toString();

        search_user(email);
    }

    /**
     * Search user in server
     * @param email from searched user
     */
    private void search_user(final String email) {
        UserService userService = ServiceGenerator.createSecureService(UserService.class);
        userService.getUserByMail(email).enqueue(new Callback<DataResponse<User>>() {
            @Override
            public void onResponse(Response<DataResponse<User>> response) {
                mView.setVisibility(View.VISIBLE);
                if (response.body()!=null) {
                    username.setVisibility(View.VISIBLE);
                    picture.setVisibility(View.VISIBLE);
                    notFound.setVisibility(View.INVISIBLE);
                    user = response.body().getData();
                    username.setText(user.getName());
                    Picasso.with(AddUserDialog.this.getContext()).load(ServiceGenerator.PROFILE_PHOTO_URL + user.getId()).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE)
                            .placeholder(AddUserDialog.this.getResources().getDrawable(R.drawable.ic_user_circle_black))
                            .error(AddUserDialog.this.getResources().getDrawable(R.drawable.ic_user_circle_black))
                            .resize(50, 50)
                            .centerCrop().transform(new CircleTransform()).into(picture);
                }else{
                    username.setVisibility(View.INVISIBLE);
                    picture.setVisibility(View.INVISIBLE);
                    notFound.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar snackbar = Snackbar
                        .make((View) mView.getParent(), getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                search_user(email);
                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        void getUser(User user, int position);

    }


}
