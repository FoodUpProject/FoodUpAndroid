package org.foodup.foodup.dialogs;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.foodup.foodup.R;
import org.foodup.foodup.beans.Aliment;
import org.foodup.foodup.beans.Ingredient;

import java.util.ArrayList;

/**
 * Dialog that allows user to add new ingredient to its dish
 */
public class AddIngredientDialog extends DialogFragment implements AdapterView.OnItemClickListener {

    private OnDialogInteractionListener mListener;
    private AutoCompleteTextView search_aliment;
    private ArrayList<Aliment> alimentsData;
    private Ingredient ingredient;
    private TextView ingredientName;
    private EditText ingredientQuantity;
    private Spinner ingredientUnit;
    private CheckBox ingredientMain;
    private LinearLayout alimentDetail;

    private int[] unitsPositionsValues;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_add_ingredients, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(v);

        builder.setPositiveButton(R.string.add, null);
        builder.setNegativeButton(R.string.cancel, null);

        ingredient = new Ingredient();

        ingredientName = (TextView) v.findViewById(R.id.ingredient_name);
        ingredientQuantity = (EditText) v.findViewById(R.id.ingredient_quantity);
        ingredientUnit = (Spinner) v.findViewById(R.id.ingredient_unit);
        ingredientMain = (CheckBox) v.findViewById(R.id.main_ingredient);
        alimentDetail = (LinearLayout) v.findViewById(R.id.ingredient_content);

        search_aliment = (AutoCompleteTextView) v.findViewById(R.id.search_aliment);
        setAllAlimentsInfo(v.getContext());
        search_aliment.setOnItemClickListener(this);

        unitsPositionsValues = getResources().getIntArray(R.array.unitsPositionsValues);


        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button positiveButton = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        if (buildIngredient()) {
                            dialog.dismiss();
                        }
                    }
                });
            }
        });

        return dialog;
    }

    private void setAllAlimentsInfo(Context context) {
        alimentsData = mListener.getAllAliments();
        if (alimentsData.size() > 0) {
            String[] alimentNames = new String[alimentsData.size()];
            for (int i = 0; i < alimentsData.size(); i++) {
                alimentNames[i] = alimentsData.get(i).getName();
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    context,
                    android.R.layout.simple_list_item_1,
                    alimentNames);
            search_aliment.setAdapter(adapter);
        } else {
            alimentsData.clear();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Aliment alimentSelected = new Aliment();
        for (Aliment aliment : alimentsData) {
            if (aliment.getName().equals(parent.getItemAtPosition(position))) {
                alimentSelected = aliment;
            }
        }

        hideSoftKeyboard();
        ingredientName.setText(alimentSelected.getName());
        ingredient.setAliment(alimentSelected);
        search_aliment.setVisibility(View.GONE);
        alimentDetail.setVisibility(View.VISIBLE);
    }

    private boolean buildIngredient() {
        if (ingredient.getAliment() != null) {
            String quantity = ingredientQuantity.getText().toString();
            if (!quantity.isEmpty()) {
                ingredient.setQuantity(Float.parseFloat(quantity));
                ingredient.setUnit(unitsPositionsValues[ingredientUnit.getSelectedItemPosition()]);
                ingredient.setMainIngredient(ingredientMain.isChecked());
                if (mListener.addIngredient(ingredient)) {
                    return true;
                }
                showError(getString(R.string.error_duplicate_ingredient));
            } else {
                ingredientQuantity.setError(getString(R.string.error_field_required));
                ingredientQuantity.requestFocus();
            }
        } else {
            search_aliment.setError(getString(R.string.error_field_required));
            search_aliment.requestFocus();
        }

        return false;
    }

    private void showError(String msg) {
        Snackbar snackbar = Snackbar
                .make(getDialog().findViewById(R.id.view), msg, Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDialogInteractionListener) {
            mListener = (OnDialogInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnDialogInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnDialogInteractionListener {

        ArrayList<Aliment> getAllAliments();

        boolean addIngredient(Ingredient ingredient);
    }
}
