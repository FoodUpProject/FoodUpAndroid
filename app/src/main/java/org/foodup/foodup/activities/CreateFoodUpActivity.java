package org.foodup.foodup.activities;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.foodup.foodup.R;
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.FoodUp;
import org.foodup.foodup.beans.User;
import org.foodup.foodup.dialogs.AddUserDialog;
import org.foodup.foodup.fragments.CreateFoodUpFirstFragment;
import org.foodup.foodup.fragments.CreateFoodUpSecondFragment;
import org.foodup.foodup.fragments.CreateFoodUpThirdFragment;
import org.foodup.foodup.fragments.DishListFragment;
import org.foodup.foodup.interfaces.FoodUpService;
import org.foodup.foodup.utils.ImageConverter;
import org.foodup.foodup.utils.ProgressDialogUtility;
import org.foodup.foodup.utils.ServiceGenerator;
import org.foodup.foodup.utils.SharedPreferencesManager;
import org.joda.time.Days;
import org.joda.time.LocalDate;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Response;

/**
 * Provides steps to choose options in order to create a FoodUp
 */
public class CreateFoodUpActivity extends AppCompatActivity implements View.OnClickListener,
        CreateFoodUpFirstFragment.OnFragmentInteractionListener,
        CreateFoodUpSecondFragment.OnFragmentInteractionListener,
        CreateFoodUpThirdFragment.OnFragmentInteractionListener, AddUserDialog.OnFragmentInteractionListener {

    private static final float FLOATING_BUTTON_MARGIN_DP = 70;
    private int position;
    private Button nextButton;
    private ArrayList<Fragment> createFoodUpFragments;
    private FoodUp foodUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_food_up);

        //set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.create_new_foodup);
        }
        ImageButton imageButton = (ImageButton) findViewById(R.id.close_button);
        if (imageButton != null) {
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }

        //get other foodups for spinner
        ArrayList<FoodUp> foodups =
                (ArrayList<FoodUp>) getIntent().getExtras().getSerializable("foodups");

        //init fragments
        createFoodUpFragments = new ArrayList<>();

        CreateFoodUpFirstFragment createFoodUpFirstFragment = new CreateFoodUpFirstFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("foodups", foodups);
        createFoodUpFirstFragment.setArguments(bundle);
        CreateFoodUpSecondFragment createFoodUpSecondFragment = new CreateFoodUpSecondFragment();
        CreateFoodUpThirdFragment createFoodUpThirdFragment = new CreateFoodUpThirdFragment();

        createFoodUpFragments.add(createFoodUpFirstFragment);
        createFoodUpFragments.add(createFoodUpSecondFragment);
        createFoodUpFragments.add(createFoodUpThirdFragment);

        //init fragment
        position = 0;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.create_content, createFoodUpFragments.get(position), "First")
                .addToBackStack(null).commit();

        //set next button
        nextButton = (Button) findViewById(R.id.next_button);
        if (nextButton != null) {
            nextButton.setEnabled(false);
            nextButton.setBackgroundResource(android.R.color.darker_gray);
            nextButton.setOnClickListener(this);
        }

        //create foodup
        foodUp = new FoodUp();

    }

    /**
     * Checks if floating button is hover user config button
     */
    @Override
    public void checkFloatingButtonPosition(RecyclerView peopleList, FloatingActionButton addButton) {
        int listHeight = peopleList.getHeight();
        Log.d("listHeight", "" + listHeight);

        int itemHeight = peopleList.getChildAt(0).getHeight();
        Log.d("itemHeight", "" + itemHeight);

        if (itemHeight * peopleList.getAdapter().getItemCount() >= listHeight - itemHeight) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) addButton.getLayoutParams();
            params.rightMargin = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    FLOATING_BUTTON_MARGIN_DP,
                    getResources().getDisplayMetrics()
            );
        } else {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) addButton.getLayoutParams();
            params.rightMargin = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    getResources().getDimension(R.dimen.floatingButtonMargin),
                    getResources().getDisplayMetrics()
            );
        }
    }

    /**
     * Sets click listener
     */
    @Override
    public void onClick(View view) {
        int nextPosition = position + 1;
        if (nextPosition < createFoodUpFragments.size() && nextButton.isEnabled()) {
            getSupportFragmentManager().beginTransaction().replace(R.id.create_content, createFoodUpFragments.get(nextPosition)).addToBackStack(null).commit();
            nextButton.setEnabled(false);
            nextButton.setBackgroundResource(android.R.color.darker_gray);
            position = nextPosition;
        }

        if (nextPosition == createFoodUpFragments.size()) {
            (new ServerTask()).execute();
        }
    }

    /**
     * Get FoodUpType from fragment
     */
    @Override
    public void getFoodUpType(int selectedButton) {
        if (selectedButton != -1) {
            foodUp.setFoodUpType(selectedButton + 1);
            nextButton.setEnabled(true);
            nextButton.setBackgroundResource(android.R.color.holo_green_light);
        } else {
            nextButton.setEnabled(false);
            nextButton.setBackgroundResource(android.R.color.darker_gray);
        }
    }

   /*upgrade
    @Override
    public void getFollowingFoodUp(int idFoodUp) {
        if (idFoodUp != -1) {
            foodUp.setFollowingFoodUp(idFoodUp);
        }
    }*/

    /**
     * Get users from fragment
     */
    @Override
    public void getUsers(ArrayList<User> users) {
        if (users != null && users.size() != 0 && position != 2) {
            foodUp.setDiners(users);
            nextButton.setEnabled(true);
            nextButton.setBackgroundResource(android.R.color.holo_green_light);
        } else {
            nextButton.setEnabled(false);
            nextButton.setBackgroundResource(android.R.color.darker_gray);
        }
    }

    /**
     * Finishes activity with result
     * @param foodUp to send
     */
    public void finishCreateFoodUp(FoodUp foodUp) {
        Intent intent = new Intent();
        intent.putExtra("foodUp", foodUp);
        setResult(RESULT_OK, intent);
        finish();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        createFoodUpFragments.get(1).onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Get FoodUp info from fragment
     */
    @Override
    public void getFoodUpInfo(String foodUpName, int color, String initDate, String endDate, boolean checked) {
        if (foodUpName != null && !foodUpName.equals("") && initDate != null && !initDate.equals("--/--/----") && (!endDate.equals("--/--/----") || checked) && color != -1) {
            foodUp.setName(foodUpName);
            foodUp.setColor(color);
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            try {
                foodUp.setIniDate(format.parse(initDate).getTime());
                if (checked) {
                    foodUp.setDaysOfMenu(-1);
                } else {
                    foodUp.setEndDate(format.parse(endDate).getTime());
                    foodUp.setDaysOfMenu(Days.daysBetween(
                            new LocalDate(foodUp.getIniDate()),
                            new LocalDate(foodUp.getEndDate())
                    ).getDays() + 1);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            nextButton.setEnabled(true);
            nextButton.setBackgroundResource(android.R.color.holo_green_light);
        } else {
            nextButton.setEnabled(false);
            nextButton.setBackgroundResource(android.R.color.darker_gray);
        }
    }

    /**
     * Proper navigation with backpressed
     */
    @Override
    public void onBackPressed() {
        position--;
        if (position == -1) {
            finish();
        }
        super.onBackPressed();
    }

    /**
     * Get user from fragment
     */
    @Override
    public void getUser(User user, int position) {
        ((CreateFoodUpSecondFragment) createFoodUpFragments.get(1)).getUser(user, position);
    }

    private void setImageFileUser(ArrayList<User> users) {
        for (User user : users) {
            String path = user.getPhoto();
            if (path != null) {
                user.setPhotoFile(ImageConverter.convertBitmapToByte(this, Uri.parse(path)));
                user.setPhoto(null);
            }
        }
    }

    /**
     * Send info to server
     */
    class ServerTask extends AsyncTask<Void, Void, FoodUp> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialogUtility.getProgressDialog(CreateFoodUpActivity.this);
            progressDialog.setCanceledOnTouchOutside(false);
            if (progressDialog != null) {
                progressDialog.setMessage("Cocinando su menú, por favor, espere...");
                progressDialog.show();
            }

            if (SharedPreferencesManager.getIdUser() > 0) {
                //obtiene los bytes de las imagenes de los usuarios
                setImageFileUser(foodUp.getDiners());
            }
        }

        @Override
        protected FoodUp doInBackground(Void... arg0) {

            try {
                Response<DataResponse<FoodUp>> response;

                if (SharedPreferencesManager.getIdUser() > 0) {
                    //obtiene los bytes de las imagenes de los usuarios
                    setImageFileUser(foodUp.getDiners());

                    FoodUpService foodUpService = ServiceGenerator.createSecureService(FoodUpService.class);
                    response = foodUpService.setFoodUp(foodUp).execute();
                } else {
                    FoodUpService foodUpService = ServiceGenerator.createService(FoodUpService.class);
                    response = foodUpService.setFoodUpExplorer(foodUp).execute();
                }

                Log.i(DishListFragment.class.getName(), "Code: " + response.code());
                Log.i(DishListFragment.class.getName(), "Message: " + response.message());

                if (response.isSuccess()) {
                    return response.body().getData();
                }
            } catch (IOException e) {
                Log.d(MainActivity.class.getName(), "Error get user from server", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(FoodUp result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null) {
                finishCreateFoodUp(result);
            } else {
                Snackbar snackbar = Snackbar
                        .make(findViewById(android.R.id.content), getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                (new ServerTask()).execute();
                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
}
