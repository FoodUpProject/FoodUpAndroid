package org.foodup.foodup.activities;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.foodup.foodup.R;
import org.foodup.foodup.adapters.MenuAdapter;
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.Dish;
import org.foodup.foodup.beans.FoodUp;
import org.foodup.foodup.beans.Meal;
import org.foodup.foodup.interfaces.FoodUpService;
import org.foodup.foodup.utils.Commons;
import org.foodup.foodup.utils.ProgressDialogUtility;
import org.foodup.foodup.utils.ServiceGenerator;
import org.foodup.foodup.utils.SharedPreferencesManager;

import java.util.LinkedHashSet;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

public class MenuFoodUpActivity extends AppCompatActivity {


    private List<Meal> meals;
    private MenuAdapter mAdapter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_food_up);

        LinkedHashSet<String> dates = new LinkedHashSet<>();

        //receive foodup
        FoodUp foodUp = (FoodUp) getIntent().getExtras().get("foodup");

        //retrieve foodup info
        if (foodUp != null) {
            meals = foodUp.getMeals();

            for (Meal meal : meals) {
                dates.add(Commons.longToString(meal.getDay()));
            }
        }

        //init toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            TextView name = (TextView) findViewById(R.id.name);
            if (foodUp != null) {
                if (name != null) {
                    name.setText(foodUp.getName());

                }
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Commons.getColor(foodUp.getColor(), this)));
            }
        }
        ImageButton imageButton = (ImageButton) findViewById(R.id.close_button);
        if (imageButton != null) {
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }


        //init dates
        RecyclerView menuView = (RecyclerView) findViewById(R.id.menu);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        if (menuView != null) {
            menuView.setLayoutManager(mLayoutManager);
            mAdapter = new MenuAdapter(dates, meals, this);
            menuView.setAdapter(mAdapter);
        }

    }

    /**
     * Replaces selected position with selected meal
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 19) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                int position = extras.getInt("position");
                Log.d("position", position + "");
                Integer idMeal = extras.getInt("idMeal");
                Log.d("idMeal", idMeal + "");
                Dish dish = (Dish) extras.get("dish");
                Log.d("dish", ""+(dish!=null));
                for (Meal meal : meals) {
                    if (meal.getId().equals(idMeal)) {
                        if (SharedPreferencesManager.getIdUser() != 0) {
                            updateDishOfMenu(meal, position, dish);
                        } else {
                            meal.getDishes().set(position, dish);
                            mAdapter.notifyDataSetChanged();
                        }
                        break;
                    }
                }
            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    /**
     * Update dish from server
     */
    private void updateDishOfMenu(final Meal meal, final int position, final Dish dish) {

        progressDialog = ProgressDialogUtility.getProgressDialog(MenuFoodUpActivity.this);
        progressDialog.setCanceledOnTouchOutside(false);
        if (progressDialog != null) {
            progressDialog.setMessage("Guardando cambios, por favor, espere...");
            progressDialog.show();
        }

        FoodUpService foodupService = ServiceGenerator.createSecureService(FoodUpService.class);
        foodupService.updateMenuDish(meal.getId(), meal.getDishes().get(position).getId(), dish.getId()).enqueue(new Callback<DataResponse<Boolean>>() {
            @Override
            public void onResponse(Response<DataResponse<Boolean>> response) {
                if (response.isSuccess()) {
                    if (response.body().getData()) {
                        meal.getDishes().set(position, dish);
                        mAdapter.notifyDataSetChanged();
                    } else {
                        Log.i(MenuFoodUpActivity.class.getName(), "No se ha podido updatear el dish ");
                    }
                }
                Log.i(MenuFoodUpActivity.class.getName(), "Code: " + response.code());
                Log.i(MenuFoodUpActivity.class.getName(), "Message: " + response.message());

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                Log.i(MenuFoodUpActivity.class.getName(), t.getMessage(), t);
                Snackbar snackbar = Snackbar
                        .make(findViewById(android.R.id.content), getString(R.string.no_connection), Snackbar.LENGTH_LONG);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        });
    }

}
