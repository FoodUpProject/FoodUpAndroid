package org.foodup.foodup.activities;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import org.foodup.foodup.R;
import org.foodup.foodup.fragments.DishListFragment;

/**
 * Allows user to choose one dish from database and place it in its menu
 */
public class ReplaceDishActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replace_dish);

        //init toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.replace);
        }
        ImageButton imageButton = (ImageButton) findViewById(R.id.close_button);
        if (imageButton != null) {
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }

        Bundle bundle = new Bundle(getIntent().getExtras());
        DishListFragment dishListFragment = new DishListFragment();
        dishListFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment, dishListFragment).commit();
    }


}
