package org.foodup.foodup.activities;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.foodup.foodup.R;
import org.foodup.foodup.adapters.IngredientsAdapter;
import org.foodup.foodup.beans.Aliment;
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.Dish;
import org.foodup.foodup.beans.Ingredient;
import org.foodup.foodup.dialogs.AddIngredientDialog;
import org.foodup.foodup.dialogs.PhotoSourceDialog;
import org.foodup.foodup.fragments.DishListFragment;
import org.foodup.foodup.interfaces.AlimentService;
import org.foodup.foodup.interfaces.DishService;
import org.foodup.foodup.utils.CircleTransform;
import org.foodup.foodup.utils.Commons;
import org.foodup.foodup.utils.ImageConverter;
import org.foodup.foodup.utils.ProgressDialogUtility;
import org.foodup.foodup.utils.ServiceGenerator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import retrofit2.Response;

/**
 * Allows user to edit dish from database and save it
 */
public class EditDishActivity extends AppCompatActivity implements View.OnClickListener, AddIngredientDialog.OnDialogInteractionListener {

    private Uri imageUri;
    private int[] dishesPositionsValues;

    private Toolbar toolbar;
    private Button btnDone;
    private EditText dishName;
    private EditText dishTime;
    private EditText dishDiners;
    private EditText dishUrl;
    private EditText dishRecipe;
    private TextView ingredientsEmpty;
    private ImageView dishPicture;
    private ImageButton btnClose;
    private FloatingActionButton btnAdd;
    private RecyclerView ingredientsList;
    private Spinner dishPositions;
    private View view;

    private Dish dish;
    private IngredientsAdapter mAdapter;
    private ProgressDialog progressDialog;
    private ArrayList<Aliment> aliments;

    private boolean editDish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_dish);

        dish = (Dish) getIntent().getExtras().get("dish");

        sendCallbakAliments();

        getViewsById();
        setViewsAction();
    }

    private void getViewsById() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        dishName = (EditText) findViewById(R.id.dish_name);
        dishTime = (EditText) findViewById(R.id.dish_time);
        dishDiners = (EditText) findViewById(R.id.dish_diners);
        dishUrl = (EditText) findViewById(R.id.dish_link);
        dishRecipe = (EditText) findViewById(R.id.dish_recipe);
        dishPicture = (ImageView) findViewById(R.id.dish_picture);
        btnAdd = (FloatingActionButton) findViewById(R.id.add_ingredient);
        btnDone = (Button) findViewById(R.id.done_button);
        btnClose = (ImageButton) findViewById(R.id.close_button);
        ingredientsEmpty = (TextView) findViewById(R.id.emptyScreen);
        ingredientsList = (RecyclerView) findViewById(R.id.ingredients_list);
        dishPositions = (Spinner) findViewById(R.id.dish_positions);
        dishesPositionsValues = getResources().getIntArray(R.array.dishesPositionsValues);
        view = findViewById(R.id.view);
    }

    private void setViewsAction() {
        setSupportActionBar(toolbar);
        dishPicture.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        btnDone.setOnClickListener(this);
        btnClose.setOnClickListener(this);

        if (getSupportActionBar() != null) {
            if (dish.getId() == null) {
                getSupportActionBar().setTitle(R.string.create_new_dish);
                initializeNewDish();
            } else if (getIntent().getBooleanExtra("duplicate", false)) {
                getSupportActionBar().setTitle(R.string.duplicate_dish);
                setDefaultConfigDish();
            } else {
                editDish = true;
                getSupportActionBar().setTitle(R.string.edit_dish);
                setDefaultConfigDish();
            }
        }

        // use a linear layout manager
        ingredientsList.setLayoutManager(new LinearLayoutManager(ingredientsList.getContext()));

        // specify an adapter (see also next example)
        mAdapter = new IngredientsAdapter(dish.getIngredients(), this);
        ingredientsList.setAdapter(mAdapter);

        setVisibilityIngredients();
    }

    private void initializeNewDish() {
        dish = new Dish();
        dish.setIngredients(new ArrayList<Ingredient>());
    }

    private void setDefaultConfigDish() {
        dishName.setText(dish.getName());
        dishTime.setText(dish.getDuration()+"");
        dishDiners.setText(dish.getDiners()+"");
        int pos = getIndexFromPositions();
        if (pos != -1) {
            dishPositions.setSelection(pos);
        }
        String url = dish.getUrl();
        if (url != null && !url.isEmpty()) {
            dishUrl.setText(url);
        } else {
            dishUrl.setText("");
        }
        String recipe = dish.getRecipe();
        if (recipe != null && !recipe.isEmpty()) {
            dishRecipe.setText(recipe);
        } else {
            dishRecipe.setText("");
        }
        if (dish.getIngredients() == null) {
            dish.setIngredients(new ArrayList<Ingredient>());
        }

        if (imageUri != null) {
            Picasso.with(this).load(imageUri).resize(150, 150)
                    .centerCrop().transform(new CircleTransform()).into(dishPicture);
        } else {
            Picasso.with(this).load(Uri.parse(ServiceGenerator.DISHES_URL + dish.getIdPicture()))
                    .placeholder(getResources().getDrawable(R.drawable.ic_menu_dish))
                    .error(getResources().getDrawable(R.drawable.ic_menu_dish))
                    .resize(150, 150)
                    .centerCrop().into(dishPicture);
        }
    }

    private int getIndexFromPositions() {
        for (int i = 0; i < dishesPositionsValues.length; i++) {
            if (dishesPositionsValues[i] == dish.getPosiblePosition()) {
                return i;
            }
        }
        return -1;
    }

    private void setVisibilityIngredients() {
        if (dish.getIngredients().isEmpty()) {
            ingredientsList.setVisibility(View.GONE);
            ingredientsEmpty.setVisibility(View.VISIBLE);
        } else {
            ingredientsList.setVisibility(View.VISIBLE);
            ingredientsEmpty.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dish_picture:
                selectImage();
                break;
            case R.id.add_ingredient:
                new AddIngredientDialog().show(getSupportFragmentManager(), "AddIngredientDialog");
                break;
            case R.id.done_button:
                if (setViewToDish()) {
                    (new DishServerTask()).execute();
                    break;
                }
                Snackbar snackbar = Snackbar
                        .make(view, getString(R.string.error_save_dish), Snackbar.LENGTH_LONG);
                snackbar.show();
                break;
            case R.id.close_button:
                finish();
                break;
        }
    }

    private boolean setViewToDish() {
        dish.setName(dishName.getText().toString());
        String duration = dishTime.getText().toString();
        if (duration.matches(getString(R.string.number))) {
            dish.setDuration(Integer.parseInt(duration));
        } else {
            return false;
        }
        String diners = dishDiners.getText().toString();
        if (diners.matches(getString(R.string.number))) {
            dish.setDiners(Integer.parseInt(diners));
        } else {
            return false;
        }
        dish.setPosiblePosition(dishesPositionsValues[dishPositions.getSelectedItemPosition()]);
        dish.setUrl(dishUrl.getText().toString());
        dish.setRecipe(dishRecipe.getText().toString());

        return !dish.getIngredients().isEmpty();
    }


    private void setImageFileToDish() {
        if (imageUri != null) {
            dish.setFilePicture(ImageConverter.convertBitmapToByte(this, imageUri));
        }
    }

    private void returnBackToActivity() {
        Intent intent = new Intent();
        intent.putExtra("dish", dish);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void selectImage() {
        displayImageChooser();
    }

    /**
     * Displays a dialog chooser. User can choose select image from gallery of device or take a new
     * image from camera.
     */
    private void displayImageChooser() {
        PhotoSourceDialog dialog = new PhotoSourceDialog();
        dialog.show(getSupportFragmentManager(), "dialog");
    }

    /**
     * Send sendBroadcast to media files for scan new file(picture captured). File will be available
     * in gallery immediately.
     */
    private void addPicGallery(Uri uri) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(uri);
        sendBroadcast(mediaScanIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Uri photo;
            if (data != null) {
                photo = data.getData();
                addPicGallery(photo);
            } else {
                photo = Uri.fromFile(new File(Commons.getImagePath()));
            }
            imageUri = photo;
            Picasso.with(this).load(photo).resize(150, 150)
                    .centerCrop().into(dishPicture);

        }
    }

    @Override
    public ArrayList<Aliment> getAllAliments() {
        return aliments;
    }

    @Override
    public boolean addIngredient(Ingredient ingredient) {
        ArrayList<Ingredient> ingredients = dish.getIngredients();
        if (!ingredients.contains(ingredient)) {
            dish.getIngredients().add(ingredient);
            mAdapter.notifyItemInserted(mAdapter.getItemCount() - 1);
            setVisibilityIngredients();
            return true;
        }
        return false;
    }

    private void sendCallbakAliments() {
        try {
            aliments = (new AlimentsServerTask()).execute().get();
            if (aliments == null) {
                aliments = new ArrayList<>();
            }
        } catch (InterruptedException e) {
            Log.d(DishListFragment.class.getName(), "InterruptedException", e);
        } catch (ExecutionException e) {
            Log.d(DishListFragment.class.getName(), "ExecutionException", e);
        }
    }


    class AlimentsServerTask extends AsyncTask<Void, Void, ArrayList<Aliment>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialogUtility.getProgressDialog(EditDishActivity.this);
            progressDialog.setCanceledOnTouchOutside(false);
            if (progressDialog != null) {
                progressDialog.setMessage("Recibiendo datos de los alimentos, por favor, espere...");
                progressDialog.show();
            }
        }

        @Override
        protected ArrayList<Aliment> doInBackground(Void... arg0) {

            AlimentService alimentService = ServiceGenerator.createService(AlimentService.class);

            try {
                Response<DataResponse<ArrayList<Aliment>>> response = alimentService.getAlimentsInfo().execute();

                Log.i(EditDishActivity.class.getName(), "Code: " + response.code());
                Log.i(EditDishActivity.class.getName(), "Message: " + response.message());

                if (response.isSuccess()) {
                    Log.i(EditDishActivity.class.getName(), "Aliments size: " + response.body().getData().size());
                    return response.body().getData();
                }
            } catch (IOException e) {
                Log.d(EditDishActivity.class.getName(), "Error get user from server", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Aliment> result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
        }
    }

    class DishServerTask extends AsyncTask<Void, Void, Dish> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialogUtility.getProgressDialog(EditDishActivity.this);
            progressDialog.setCanceledOnTouchOutside(false);
            if (progressDialog != null) {
                progressDialog.setMessage("Guardando cambios, por favor, espere...");
                progressDialog.show();
            }

            setImageFileToDish();
        }

        @Override
        protected Dish doInBackground(Void... arg0) {

            DishService dishService = ServiceGenerator.createSecureService(DishService.class);

            try {
                Response<DataResponse<Dish>> response;
                if (editDish) {
                    response = dishService.setDish(dish).execute();
                } else {
                    response = dishService.createDish(dish).execute();
                }

                Log.i(EditDishActivity.class.getName(), "Code: " + response.code());
                Log.i(EditDishActivity.class.getName(), "Message: " + response.message());

                if (response.isSuccess()) {
                    Log.i(EditDishActivity.class.getName(), "Dish created or updated");
                    return response.body().getData();
                }
            } catch (IOException e) {
                Log.d(EditDishActivity.class.getName(), "Error set dish from server", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Dish result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null) {
                dish = result;
                Log.i(EditDishActivity.class.getName(), "Volver a activity");
                returnBackToActivity();
            }
        }
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
}
