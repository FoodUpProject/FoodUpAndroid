package org.foodup.foodup.activities;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.foodup.foodup.R;
import org.foodup.foodup.adapters.AllergiesOfUserAdapter;
import org.foodup.foodup.beans.Aliment;
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.User;
import org.foodup.foodup.dialogs.PhotoSourceDialog;
import org.foodup.foodup.fragments.DishListFragment;
import org.foodup.foodup.interfaces.AlimentService;
import org.foodup.foodup.interfaces.UserService;
import org.foodup.foodup.utils.CircleTransform;
import org.foodup.foodup.utils.Commons;
import org.foodup.foodup.utils.ImageConverter;
import org.foodup.foodup.utils.ProgressDialogUtility;
import org.foodup.foodup.utils.ServiceGenerator;
import org.foodup.foodup.utils.SharedPreferencesManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Callback;
import retrofit2.Response;

/**
 * Configures a user in order to know his preferences to provide a suitable menu
 */
public class ConfigUserActivity extends AppCompatActivity implements View.OnFocusChangeListener, View.OnClickListener, AdapterView.OnItemClickListener {


    private int position;
    private User user;
    private EditText name;
    private AutoCompleteTextView search_aliment;
    private RadioGroup radioButtonGroup;
    private ArrayList<Aliment> alimentsData;
    private ArrayList<Aliment> userAllergies;
    private ImageView picture;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_user);

        //get user & its position
        position = getIntent().getExtras().getInt("position", 0);
        user = (User) getIntent().getExtras().get("configUser");

        setDefaultConfigUser();

        //init toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.config_user);
        }
        ImageButton imageButton = (ImageButton) findViewById(R.id.close_button);
        if (imageButton != null) {
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        }

        // use a linear layout manager
        RecyclerView allergies = (RecyclerView) findViewById(R.id.allergies);
        if (allergies != null) {
            allergies.setLayoutManager(new LinearLayoutManager(allergies.getContext()));
            // specify an adapter
            RecyclerView.Adapter mAdapter = new AllergiesOfUserAdapter(userAllergies);
            allergies.setAdapter(mAdapter);
        }

        //init pic
        picture = (ImageView) findViewById(R.id.user_picture);
        if (user.getId() == null || user.getId().equals(SharedPreferencesManager.getIdUser())) {
            picture.setOnClickListener(this);
        }

        setImage();

        //init name
        name = (EditText) findViewById(R.id.edit_username);
        if (user.getName() != null && !user.getName().startsWith("Persona ")) {
            name.setText(user.getName());
        } else {
            name.setText(getString(R.string.person, (position + 1)));
        }
        name.setOnFocusChangeListener(this);

        //init options
        radioButtonGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioButtonGroup.check(radioButtonGroup.getChildAt(user.getQuantity()).getId());

        //set autocompletetextview
        search_aliment = (AutoCompleteTextView) findViewById(R.id.search_aliment);
        sendAllAlimentsInfoRequest();
        search_aliment.setOnItemClickListener(this);

        //set next button
        Button nextButton = (Button) findViewById(R.id.next_button);
        if (nextButton != null) {
            nextButton.setOnClickListener(this);
        }
    }

    /**
     * Displays a dialog chooser. User can choose select image from gallery of device or take a new
     * image from camera.
     */
    private void displayImageChooser() {
        PhotoSourceDialog dialog = new PhotoSourceDialog();
        dialog.show(getSupportFragmentManager(), "dialog");
    }

    /**
     * Send sendBroadcast to media files for scan new file(picture captured). File will be available
     * in gallery immediately.
     */
    private void addPicGallery(Uri uri) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(uri);
        sendBroadcast(mediaScanIntent);
    }

    /**
     * Get picture from gallery or camera and set it on layout
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Uri photo;
            if (data != null) {
                photo = data.getData();
                addPicGallery(photo);
            } else {
                photo = Uri.fromFile(new File(Commons.getImagePath()));
            }
            user.setPhoto(photo.toString());
            Picasso.with(this).load(photo).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).resize(150, 150)
                    .centerCrop().transform(new CircleTransform()).into(picture);

        }
    }

    private void setDefaultConfigUser() {
        userAllergies = user.getAllergies();
        if (userAllergies == null) {
            userAllergies = new ArrayList<>();
            user.setAllergies(userAllergies);
        }
        if (user.getIntolerance() == null) {
            user.setIntolerance(-1);
        }
        if (user.getQuantity() == null) {
            user.setQuantity(2);
        }
    }

    private void setImage() {
        String photoPath = user.getPhoto();
        if (photoPath != null) {
            Picasso.with(this).load(Uri.parse(photoPath)).resize(150, 150)
                    .centerCrop().transform(new CircleTransform()).into(picture);
        } else if (user.getId() != null && user.getId() > 0) {
            Picasso.with(this).load(Uri.parse(ServiceGenerator.PROFILE_PHOTO_URL + user.getId()))
                    .error(getResources().getDrawable(R.drawable.ic_user_circle_black))
                    .resize(150, 150)
                    .centerCrop().transform(new CircleTransform()).into(picture);
        }
    }

    /**
     * Save info in server
     *
     * @param view clicked view
     */
    @Override
    public void onClick(View view) {
        if (view.equals(picture)) {
            displayImageChooser();
        } else {
            int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
            View radioButton = radioButtonGroup.findViewById(radioButtonID);
            int radioButtonIndex = radioButtonGroup.indexOfChild(radioButton);

            user.setQuantity(radioButtonIndex);
            user.setName(name.getText().toString());

            // only saving configuration in the following cases:
            // - configured user is the app's user
            // - configured user is one created by app's user (without mail in server)
            // in other cases it's saved in memory until the FoodUp's end
            if (user.getId() != null && user.getId() > 0 && (user.getId() == SharedPreferencesManager.getIdUser() || user.getMail() == null)) {
                (new ServerTask()).execute();
            } else {
                returnBackToActivity(user);
            }
        }
    }

    /**
     * Request aliments to server
     */
    public void sendAllAlimentsInfoRequest() {
        ServiceGenerator.createService(AlimentService.class).getAlimentsInfo()
                .enqueue(new Callback<DataResponse<ArrayList<Aliment>>>() {

                    @Override
                    public void onResponse(Response<DataResponse<ArrayList<Aliment>>> response) {
                        if (response.body().getData() != null) {
                            alimentsData = response.body().getData();
                            Log.i("sendAllAlimentsInfo", "" + response.body().getData().size());
                            if (alimentsData.size() > 0) {
                                String[] alimentNames = new String[alimentsData.size()];
                                for (int i = 0; i < alimentsData.size(); i++) {
                                    alimentNames[i] = alimentsData.get(i).getName();
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<>(
                                        ConfigUserActivity.this,
                                        android.R.layout.simple_list_item_1,
                                        alimentNames);
                                search_aliment.setAdapter(adapter);
                            } else {
                                alimentsData.clear();
                            }
                        } else {
                            Log.i("sendAllAlimentsInfo", "NULL");
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        //show error message and button to retry
                        Snackbar snackbar = Snackbar
                                .make(findViewById(android.R.id.content), getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                                .setAction(getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        sendAllAlimentsInfoRequest();
                                    }
                                });

                        // Changing message text color
                        snackbar.setActionTextColor(Color.RED);

                        // Changing action button text color
                        View sbView = snackbar.getView();
                        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setTextColor(Color.YELLOW);
                        snackbar.show();
                    }

                });
    }

    /**
     * Displays aliment chosen by user
     */
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Aliment alergy = new Aliment();
        for (Aliment aliment : alimentsData) {
            if (aliment.getName().equals(adapterView.getItemAtPosition(i))) {
                alergy = aliment;
            }
        }

        if (!userAllergies.contains(alergy)) {
            userAllergies.add(alergy);
        }
        hideSoftKeyboard();
        search_aliment.setText("");
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Finishes the activity with result
     *
     * @param user configured user
     */
    private void returnBackToActivity(User user) {
        Intent intent = new Intent();
        intent.putExtra("configUser", user);
        intent.putExtra("position", position);
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * Hides soft keyboard when focus is changed
     */
    @Override
    public void onFocusChange(View view, boolean b) {
        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideSoftKeyboard();
                }
            }
        });
    }


    private void setImageFileUser() {
        String photoPath = user.getPhoto();
        if (photoPath != null) {
            user.setPhotoFile(ImageConverter.convertBitmapToByte(this, Uri.parse(photoPath)));
            user.setPhoto(null);
        }
    }

    /**
     * Send info to server
     */
    class ServerTask extends AsyncTask<Void, Void, User> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialogUtility.getProgressDialog(ConfigUserActivity.this);
            progressDialog.setCanceledOnTouchOutside(false);
            if (progressDialog != null) {
                progressDialog.setMessage("Guardando su configuración, por favor, espere...");
                progressDialog.show();
            }

            setImageFileUser();
        }

        @Override
        protected User doInBackground(Void... arg0) {

            try {

                UserService userService = ServiceGenerator.createSecureService(UserService.class);
                Response<DataResponse<User>> response = userService.setUser(user).execute();

                Log.i(DishListFragment.class.getName(), "Code: " + response.code());
                Log.i(DishListFragment.class.getName(), "Message: " + response.message());

                if (response.isSuccess()) {
                    return response.body().getData();
                }
            } catch (IOException e) {
                Log.d(MainActivity.class.getName(), "Error get user from server", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(User result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null) {
                returnBackToActivity(result);
            } else {
                Snackbar snackbar = Snackbar
                        .make(findViewById(android.R.id.content), getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                (new ServerTask()).execute();
                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        }
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }
}
