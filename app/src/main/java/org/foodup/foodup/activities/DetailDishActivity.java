package org.foodup.foodup.activities;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.foodup.foodup.R;
import org.foodup.foodup.adapters.IngredientsAdapter;
import org.foodup.foodup.beans.Dish;
import org.foodup.foodup.beans.Ingredient;
import org.foodup.foodup.dialogs.BecomeUserDialog;
import org.foodup.foodup.utils.ServiceGenerator;
import org.foodup.foodup.utils.SharedPreferencesManager;

import java.util.ArrayList;

public class DetailDishActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_EDIT = 3;
    private static final int REQUEST_CODE_DUPLICATE = 4;

    public String[] dishesPositions;
    public int[] dishesPositionsValues;
    private Toolbar toolbar;

    //Views
    private TextView dishTime;
    private TextView dishUrl;
    private TextView dishRecipe;
    private TextView ingredientsEmpty;
    private ImageView dishPicture;
    private ImageButton btnClose;
    private RecyclerView ingredientsList;
    private TextView dishPosition;
    private TextView dishDiners;
    private ImageButton viewIngredients;
    private Dish dish;
    private ImageView editButton;
    private ImageView duplicateButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_dish);

        dish = (Dish) getIntent().getExtras().get("dish");

        getViewsById();
        setViewsAction();
    }

    /**
     * init views
     */
    private void getViewsById() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        dishDiners = (TextView) findViewById(R.id.dish_diners);
        dishTime = (TextView) findViewById(R.id.dish_time);
        dishUrl = (TextView) findViewById(R.id.dish_link);
        dishRecipe = (TextView) findViewById(R.id.dish_recipe);
        ingredientsEmpty = (TextView) findViewById(R.id.emptyScreen);
        dishPicture = (ImageView) findViewById(R.id.dish_picture);
        editButton = (ImageView) findViewById(R.id.edit_button);
        duplicateButton = (ImageView) findViewById(R.id.duplicate_button);
        btnClose = (ImageButton) findViewById(R.id.close_button);
        ingredientsList = (RecyclerView) findViewById(R.id.ingredients_list);
        dishPosition = (TextView) findViewById(R.id.dish_position);
        dishesPositionsValues = getResources().getIntArray(R.array.dishesPositionsValues);
        dishesPositions = getResources().getStringArray(R.array.dishesPositions);
        viewIngredients = (ImageButton) findViewById(R.id.view_ingredients);
    }

    /**
     * set listeners
     */
    private void setViewsAction() {
        editButton.setOnClickListener(this);
        duplicateButton.setOnClickListener(this);
        btnClose.setOnClickListener(this);
        viewIngredients.setOnClickListener(this);

        setSupportActionBar(toolbar);

        setInfoToView();
    }

    /**
     * Sets info of views
     */
    private void setInfoToView() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(dish.getName());
        }
        dishDiners.setText(dish.getDiners()+"");
        dishTime.setText(dish.getDuration()+"");
        String url = dish.getUrl();
        if (url != null && !url.isEmpty()) {
            dishUrl.setText(url);
        } else {
            dishUrl.setText(R.string.no_url_recipe);
        }
        String recipe = dish.getRecipe();
        if (recipe != null && !recipe.isEmpty()) {
            dishRecipe.setText(recipe);
        } else {
            dishRecipe.setText(getText(R.string.no_recipe));
        }
        if (dish.getIngredients() == null) {
            dish.setIngredients(new ArrayList<Ingredient>());
            ingredientsList.setVisibility(View.GONE);
            viewIngredients.setVisibility(View.GONE);
            ingredientsEmpty.setVisibility(View.VISIBLE);
        } else {
            viewIngredients.setVisibility(View.VISIBLE);
            ingredientsList.setVisibility(View.GONE);
            ingredientsEmpty.setVisibility(View.GONE);
        }

        if (dish.getIdUserOwner() == SharedPreferencesManager.getIdUser()) {
            editButton.setVisibility(View.VISIBLE);
            duplicateButton.setVisibility(View.GONE);
        } else {
            editButton.setVisibility(View.GONE);
            duplicateButton.setVisibility(View.VISIBLE);
        }

        int pos = getIndexFromPositions();
        if (pos != -1) {
            dishPosition.setText(dishesPositions[pos]);
        }

        Picasso.with(this).load(Uri.parse(ServiceGenerator.DISHES_URL + dish.getIdPicture()))
                .placeholder(getResources().getDrawable(R.drawable.ic_menu_dish))
                .error(getResources().getDrawable(R.drawable.ic_menu_dish))
                .resize(150, 150)
                .centerCrop().into(dishPicture);

        // use a linear layout manager
        ingredientsList.setLayoutManager(new LinearLayoutManager(ingredientsList.getContext()));

        // specify an adapter (see also next example)
        IngredientsAdapter mAdapter = new IngredientsAdapter(dish.getIngredients(), this, true);
        ingredientsList.setAdapter(mAdapter);
    }

    /**
     * Get posible position of dish
     *
     * @return posible position
     */
    private int getIndexFromPositions() {
        for (int i = 0; i < dishesPositionsValues.length; i++) {
            if (dishesPositionsValues[i] == dish.getPosiblePosition()) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Click listeners
     *
     * @param v clicked view
     */
    @Override
    public void onClick(View v) {
        final boolean isExplorer = SharedPreferencesManager.getIdUser() != 0;
        switch (v.getId()) {
            case R.id.edit_button:
                startActivityForResult(new Intent(this, EditDishActivity.class).
                                putExtra("duplicate", false).
                                putExtra("dish", dish),
                        REQUEST_CODE_EDIT);
                break;
            case R.id.duplicate_button:
                if (isExplorer) {
                    startActivity(new Intent(this, EditDishActivity.class).
                            putExtra("duplicate", true).
                            putExtra("dish", dish));
                } else {
                    BecomeUserDialog dialog = new BecomeUserDialog();
                    dialog.show(getSupportFragmentManager(), "dialog");
                }
                break;
            case R.id.close_button:
                finish();
                break;
            case R.id.view_ingredients:
                viewIngredients.setVisibility(View.GONE);
                ingredientsList.setVisibility(View.VISIBLE);
                break;
        }
    }

    /**
     * Get and manage data from intent
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_EDIT) {
                dish = (Dish) data.getExtras().get("dish");
                setInfoToView();
            } else if (requestCode == REQUEST_CODE_DUPLICATE) {
                finish();
            }
        }
    }
}
