package org.foodup.foodup.activities;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.foodup.foodup.R;
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.User;
import org.foodup.foodup.dialogs.WelcomeExplorerDialog;
import org.foodup.foodup.fragments.DishListFragment;
import org.foodup.foodup.fragments.FoodUpListFragment;
import org.foodup.foodup.interfaces.UserService;
import org.foodup.foodup.utils.CircleTransform;
import org.foodup.foodup.utils.CurrentUser;
import org.foodup.foodup.utils.ProgressDialogUtility;
import org.foodup.foodup.utils.ServiceGenerator;
import org.foodup.foodup.utils.SharedPreferencesManager;

import java.io.IOException;

import retrofit2.Response;

/**
 * Principal activity with user information
 * and has a drawer layout that redirects to desired fragment
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int GO_PROFILE = 1;
    protected ProgressDialog progressDialog;
    private NavigationView navigationView;
    private FoodUpListFragment foodUpListFragment;
    private boolean main;
    private boolean newUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        newUser = getIntent().getBooleanExtra("newUser", false);
        if (newUser) {
            goToProfileUser();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set user info if its a real user
        if (SharedPreferencesManager.getIdUser() == 0) {
            WelcomeExplorerDialog dialog = new WelcomeExplorerDialog();
            dialog.show(getSupportFragmentManager(), "dialog");
        }

        //init toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.foodups);
        }

        //set animation opening drawer layout
        final CoordinatorLayout mainContent = (CoordinatorLayout) findViewById(R.id.main_content);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (mainContent != null) {
                    mainContent.setTranslationX(slideOffset * drawerView.getWidth());
                }
            }
        };

        //init drawer
        if (drawer != null) {
            drawer.addDrawerListener(toggle);
        }
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }
        if (navigationView != null) {
            navigationView.getMenu().getItem(0).setChecked(true);
            //header with users profile
            navigationView.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToProfileUser();
                }
            });
        }

        //if the user doesn't have name is because there is a token,
        // so that, we must retrieve data from server with a secure call
        if (CurrentUser.get().getName() == null && CurrentUser.get().getId() != 0) {
            (new ServerTask()).execute();
        } else {
                setInfoUserOnView();
        }
        goToFoodUpsUser();

    }

    /**
     * Sets proper navigation
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (!main) {
                goToFoodUpsUser();
            } else {
                super.onBackPressed();
            }
        }
    }

    /**
     * Select desired fragment from drawer
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation card item clicks here.
        int id = item.getItemId();

        if (id == R.id.foodups) {
            // Handle the camera action
            foodUpListFragment = new FoodUpListFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content, foodUpListFragment, "FoodUp's").commit();
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(R.string.foodups);
                main = true;
            }
            //future implementations:
       /* } else if (id == R.id.shopping_list) {
            /*ShoppingListFragment shoppingListFragment = new ShoppingListFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content, shoppingListFragment, "Shopping List").commit();
            */
           /* if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(R.string.shopping_list);
            }*/
        } else if (id == R.id.dishes) {
            DishListFragment dishListFragment = new DishListFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.content, dishListFragment, "Dishes List").commit();
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(R.string.dishes);
                main = false;
            }
            //future implementations:
        /*} else if (id == R.id.contacts) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(R.string.contacts);
            }
        } else if (id == R.id.settings) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(R.string.settings);
            }*/
        } else if (id == R.id.logout) {
            logout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    /**
     * Get and manage data from intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GO_PROFILE) {
            if (resultCode == RESULT_OK) {
                CurrentUser.set((User) data.getExtras().get("configUser"));
                setInfoUserOnView();
                goToFoodUpsUser();
            } else {
                if (newUser) {
                    newUser = false;
                    setInfoUserOnView();
                    goToFoodUpsUser();
                }
                //Snackbar.make(findViewById(android.R.id.content), getString(R.string.no_save), Snackbar.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Starts ConfigUserActivity waiting for result
     */
    private void goToProfileUser() {
        Intent intent = new Intent(this, ConfigUserActivity.class);
        intent.putExtra("configUser", CurrentUser.get());
        startActivityForResult(intent, GO_PROFILE);
    }

    /**
     * Starts LoginActivity and ends this one
     */
    private void logout() {
        SharedPreferencesManager.deleteFoodUpPreferences(this);
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    /**
     * Set user's info on layout
     */
    private void setInfoUserOnView() {
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.profile_name)).setText(CurrentUser.get().getName());
        ImageView picture = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.profile_image);
        String photoPath = CurrentUser.get().getPhoto();
        if (photoPath != null) {
            Picasso.with(this).load(Uri.parse(photoPath)).resize(150, 150)
                    .centerCrop().transform(new CircleTransform()).into(picture);
        } else if (CurrentUser.get().getId() != null && CurrentUser.get().getId() > 0) {
            Picasso.with(this).load(Uri.parse(ServiceGenerator.PROFILE_PHOTO_URL + CurrentUser.get().getId())).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE)
                    .placeholder(getResources().getDrawable(R.drawable.ic_user_circle_black))
                    .error(getResources().getDrawable(R.drawable.ic_user_circle_black))
                    .resize(150, 150)
                    .centerCrop().transform(new CircleTransform()).into(picture);
        }
    }

    /**
     * Replaces current fragment for foodUp's fragment
     */
    public void goToFoodUpsUser() {
        foodUpListFragment = new FoodUpListFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content, foodUpListFragment, "FoodUp's").commit();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.foodups);
        }
        main = true;
    }

    /**
     * Get user's info from server
     */
    class ServerTask extends AsyncTask<Void, Void, User> {

        private boolean invalidUser;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialogUtility.getProgressDialog(MainActivity.this);
            progressDialog.setCanceledOnTouchOutside(false);
            if (progressDialog != null) {
                progressDialog.setMessage(getString(R.string.loading));
                progressDialog.show();
            }
        }

        @Override
        protected User doInBackground(Void... arg0) {

            UserService userService = ServiceGenerator.createSecureService(UserService.class);

            try {
                Response<DataResponse<User>> response = userService.getUser().execute();

                Log.i(DishListFragment.class.getName(), "Code: " + response.code());
                Log.i(DishListFragment.class.getName(), "Message: " + response.message());

                if (response.isSuccess()) {
                    return response.body().getData();
                } else if (response.code() == 401) {
                    invalidUser = true;
                }
            } catch (IOException e) {
                Log.d(MainActivity.class.getName(), "Error get user from server", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(User result) {

            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null) {
                CurrentUser.set(result);
                setInfoUserOnView();
            } else if (invalidUser) {
                logout();
            } else {
                Snackbar snackbar = Snackbar
                        .make(findViewById(android.R.id.content), getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                (new ServerTask()).execute();
                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        }
    }
}
