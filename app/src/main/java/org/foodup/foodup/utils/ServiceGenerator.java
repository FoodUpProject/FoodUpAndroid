package org.foodup.foodup.utils;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.util.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Util class to communicate with Retrofit
 */
public class ServiceGenerator {

    private static final String BASE_URL = "http://foodup-rest.no-ip.org:80/";
    public static final String PROFILE_PHOTO_URL = BASE_URL + "resources/users/photo/";
    public static final String DISHES_URL = BASE_URL + "resources/pictures/";
    private static final String SECRET = "secreto";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static String generateHashSha256(String message) {
        String hash = null;

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.reset();
            byte[] buffer = message.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();

            StringBuilder sb = new StringBuilder();
            for (byte aDigest : digest) {
                sb.append(Integer.toString((aDigest & 0xff) + 0x100, 16).substring(1));
            }

            hash = sb.toString();

        } catch (NoSuchAlgorithmException e) {
            Logger.getLogger(ServiceGenerator.class.getName()).log(Level.SEVERE, null, e);
        } catch (UnsupportedEncodingException e) {
            Logger.getLogger(ServiceGenerator.class.getName()).log(Level.SEVERE, null, e);
        }

        return hash;
    }

    private static String generateAuth() {
        int idUser = SharedPreferencesManager.getIdUser();

        if (idUser > 0) {
            String token = SharedPreferencesManager.getToken();
            long currentTimeMillis = System.currentTimeMillis();
            String hash = generateHashSha256(currentTimeMillis + SECRET + token);
            return Base64.encodeToString((idUser + ":" + currentTimeMillis + ":" + hash).getBytes(), Base64.NO_WRAP);
        }

        return null;
    }

    public static <T> T createService(Class<T> serviceClass) {

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }


    public static <T> T createSecureService(Class<T> serviceClass, String email, String password) {
        if (email != null && password != null) {
            String credentials = email + ":" + password;
            final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", basic)
                            .header("Accept", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }

        return createService(serviceClass);
    }


    public static <T> T createSecureService(Class<T> serviceClass) {
        final String auth = generateAuth();
        if (auth != null) {
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", auth)
                            .header("Accept", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }

        return createService(serviceClass);
    }

}