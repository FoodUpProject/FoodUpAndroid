package org.foodup.foodup.utils;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
import android.app.ProgressDialog;
import android.content.Context;

/**
 * Dialog displayed when loading data from server
 */
public class ProgressDialogUtility {
    public static ProgressDialog getProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog
                .setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.setProgress(0);
        return progressDialog;
    }
}
