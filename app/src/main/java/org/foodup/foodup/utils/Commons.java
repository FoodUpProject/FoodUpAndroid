package org.foodup.foodup.utils;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;

import org.foodup.foodup.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Util class with different applications
 */
public class Commons {

    /**
     * Name of directory in case of SD don't exist and a private app dir will be created
     */
    private static final String INTERNAL_DIRECTORY_STORAGE_IMAGES_NAME = "FoodUp";
    /**
     * Default date format
     */
    private static final String DATE_FORMAT = "yyyyMMdd_HHmmss";
    /**
     * File's extensions
     */
    private static final String IMAGE_FILE_EXTENSION = ".png";
    /**
     * Request codes
     */
    private static final int GALLERY = 0;
    private static final int CAMERA = 1;

    //variables
    private static String imagePath;

    /**
     * Get color of foodUp
     * @param color index of required color
     * @param context used context
     * @return color
     */
    public static int getColor(Integer color, Context context) {

        switch (color) {
            case 1:
                return context.getResources().getColor(R.color.colorPrimary);
            case 2:
                return context.getResources().getColor(R.color.foodup_green);
            case 3:
                return context.getResources().getColor(R.color.foodup_brown);
            case 4:
                return context.getResources().getColor(R.color.foodup_yellow);
            case 5:
                return context.getResources().getColor(R.color.foodup_orange);
            case 6:
                return context.getResources().getColor(R.color.foodup_red);
            case 7:
                return context.getResources().getColor(android.R.color.holo_purple);
            default:
                return context.getResources().getColor(android.R.color.darker_gray);
        }
    }

    public static String longToString(Long day) {
        long val = day;
        Date date = new Date(val);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
        return df.format(date);
    }

    /**
     * Asks to user for permissions to use the camera of device.
     * <p/>
     * Needed permissions are:
     * {android.Manifest.permission.WRITE_EXTERNAL_STORAGE}
     * {android.Manifest.permission.CAMERA}
     */
    @TargetApi(Build.VERSION_CODES.M)
    public static void askCameraPermission(Context context) {
        if (PermissionsManager.hasPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, context)) {
            ((Activity)context).requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                    PermissionsManager.CAMERA_RESULT);
        } else {
            ((Activity)context).requestPermissions(new String[]{android.Manifest.permission.CAMERA,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PermissionsManager.CAMERA_RESULT);
        }
    }

    /**
     * Asks to user for permissions to access to device storage.
     * <p/>
     * Needed permissions are:
     * {android.Manifest.permission.WRITE_EXTERNAL_STORAGE}
     */
    @TargetApi(Build.VERSION_CODES.M)
    public static void askGalleryPermission(Context context) {
        ((Activity)context).requestPermissions(
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PermissionsManager.ACCESS_GALLERY_RESULT);
    }

    /**
     * Launches browser file storage to select image file.
     */
    public static void pickImageIntent(Context context) {
        Intent intent = new Intent();
        intent.setType("image/*");

        //Intent.ACTION_OPEN_DOCUMENT is required min API19, current is 16
        if (android.os.Build.VERSION.SDK_INT > 18) {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        } else {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }

        intent.addCategory(Intent.CATEGORY_OPENABLE);
        ((Activity)context).startActivityForResult(intent, GALLERY);
    }

    /**
     * Launches device's camera.
     */
    public static void takePictureIntent(Context context) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile;
            photoFile = Commons.createImageFile(context);

            // Continue only if the File was successfully created
            if (photoFile != null) {
                imagePath = photoFile.getAbsolutePath();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                ((Activity)context).startActivityForResult(takePictureIntent, CAMERA);
            }
        }
    }

    /**
     * Creates image file for picture
     */
    public static File createImageFile(Context context) {
        try {
            return createFile(IMAGE_FILE_EXTENSION, context);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Creates File.
     * <p/>
     * Filename:  "[appname]_[creationtime]"
     *
     * @return File
     * @throws IOException
     */
    public static File createFile(String extension, Context context) throws IOException {
        // Creates a file name
        String title = "FoodUp";
        String timeStamp = new SimpleDateFormat(DATE_FORMAT).format(new Date());
        String fileName = title + "_" + timeStamp;

        File storageDir = getStorageDir(context);
        File file = File.createTempFile(
                fileName, //prefix
                extension, //suffix
                storageDir //directory
        );

        return file;
    }

    /**
     * Checks if device has a external memory and returns directory to save pictures. These directories
     * are public.
     *
     * @return File directory
     */
    private static File getStorageDir(Context context) {

        // Checks if external storage is available for read and write
        boolean haveSd = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        File storageDir;

        if (haveSd) {
            storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        } else {
            ContextWrapper cw = new ContextWrapper(context);
            storageDir = cw.getDir(INTERNAL_DIRECTORY_STORAGE_IMAGES_NAME, Context.MODE_PRIVATE);
        }

        if (storageDir != null) {
            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    return null;
                }
            }
        }
        return storageDir;
    }

    public static String getImagePath() {
        return imagePath;
    }
}
