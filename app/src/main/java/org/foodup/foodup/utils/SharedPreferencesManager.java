package org.foodup.foodup.utils;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Util class to access to SharedPreferences
 */
public class SharedPreferencesManager {

    private static final String FOODUP_PREFERENCES = "FOODUP_PREFERENCES";
    private static final String ID_USER_VALUE = "id_user";
    private static final String TOKEN_VALUE = "token";

    private static int idUser = -2;
    private static String token;


    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(FOODUP_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static void refteshPreferences(Context context) {
        idUser = getSharedPreferences(context).getInt(ID_USER_VALUE, -1);
        token = getSharedPreferences(context).getString(TOKEN_VALUE, null);
    }

    public static int getIdUser() {
        return idUser;
    }

    public static String getToken() {
        return token;
    }

    public static void setFoodupPreferences(Context context, int idUser, String token) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(ID_USER_VALUE, idUser);
        editor.putString(TOKEN_VALUE, token);
        editor.apply();

        SharedPreferencesManager.idUser = idUser;
        SharedPreferencesManager.token = token;
    }

    public static void deleteFoodUpPreferences(Context context) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.clear();
        editor.apply();
        idUser = -1;
        token = null;
        CurrentUser.set(null);
    }
}
