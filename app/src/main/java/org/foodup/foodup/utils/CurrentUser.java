package org.foodup.foodup.utils;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import org.foodup.foodup.beans.User;

/**
 * Gets and sets current user accessible from all classes
 */
public class CurrentUser {

    private static User currentUser;

    public static User get() {
        return currentUser;
    }

    public static void set(User user) {
        currentUser = user;
    }
}
