package org.foodup.foodup.utils;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;


/**
 * Util class used for permissions in api>22
 */
public class PermissionsManager {

    public final static int CAMERA_RESULT = 0;
    public final static int ACCESS_GALLERY_RESULT = 1;

    /**
     * method that will return whether the permission is accepted. By default it is true if the user
     * is using a device below version 23
     */
    public static boolean hasPermission(String permission, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
        }
        return true;
    }
}