package org.foodup.foodup.fragments;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.foodup.foodup.R;
import org.foodup.foodup.dialogs.DatePickerFragmentDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Create foodUp third step which is customizing foodUp data like name,
 * color and init and end date of menu
 */
public class CreateFoodUpThirdFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, TextWatcher, View.OnClickListener {

    private static final int INIT_DATE_CODE = 1;
    private static final int END_DATE_CODE = 2;


    private TextView endDate;
    private TextView initDate;
    private EditText foodUpName;
    private ArrayList<ImageButton> colors;
    private String name;
    private int selectedColor;
    private String endingDate;
    private String initialDate;
    private boolean checked;

    private OnFragmentInteractionListener mListener;


    public CreateFoodUpThirdFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_food_up_third, container, false);

        CheckBox indefinite = (CheckBox) view.findViewById(R.id.indefinite);
        indefinite.setOnCheckedChangeListener(this);

        foodUpName = (EditText) view.findViewById(R.id.foodup_name);
        foodUpName.addTextChangedListener(this);

        endDate = (TextView) view.findViewById(R.id.end_date);
        endDate.addTextChangedListener(this);
        endDate.setOnClickListener(this);

        initDate = (TextView) view.findViewById(R.id.init_date);
        initDate.addTextChangedListener(this);
        initDate.setOnClickListener(this);

        colors = new ArrayList<>();
        colors.add((ImageButton) view.findViewById(R.id.grey));
        colors.add((ImageButton) view.findViewById(R.id.blue));
        colors.add((ImageButton) view.findViewById(R.id.green));
        colors.add((ImageButton) view.findViewById(R.id.brown));
        colors.add((ImageButton) view.findViewById(R.id.yellow));
        colors.add((ImageButton) view.findViewById(R.id.orange));
        colors.add((ImageButton) view.findViewById(R.id.red));
        colors.add((ImageButton) view.findViewById(R.id.purple));

        colors.get(0).setPressed(true);

        for (ImageButton button : colors) {
            button.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    for (ImageButton button : colors) {
                        button.setPressed(false);
                    }
                    v.setPressed(true);
                    selectedColor = colors.indexOf(v);
                    mListener.getFoodUpInfo(name, selectedColor, initialDate, endingDate, checked);
                    return true;
                }
            });

        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        colors.get(0).setPressed(false);
        colors.get(selectedColor).setPressed(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            checked = true;
            endDate.setVisibility(View.GONE);
            mListener.getFoodUpInfo(name, selectedColor, initialDate, endingDate, checked);
        } else {
            checked = false;
            endDate.setVisibility(View.VISIBLE);
            mListener.getFoodUpInfo(name, selectedColor, initialDate, endingDate, checked);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (!foodUpName.getText().toString().equals("")) {
            name = foodUpName.getText().toString();
        }
        if (!initDate.getText().toString().equals("")) {
            initialDate = initDate.getText().toString();
        }
        if (!endDate.getText().toString().equals("")) {
            endingDate = endDate.getText().toString();
        }
        colors.get(selectedColor).setPressed(true);
        mListener.getFoodUpInfo(name, selectedColor, initialDate, endingDate, checked);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.init_date:
                showDatePickerDialog(view, INIT_DATE_CODE);
                break;
            case R.id.end_date:
                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date initDateFormat = null;
                try {
                    initDateFormat = formatter.parse(initDate.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (initDateFormat == null) {
                    Snackbar.make(getActivity().findViewById(R.id.view),
                            getString(R.string.select_init_date), Snackbar.LENGTH_SHORT).show();
                } else {
                    showDatePickerDialog(view, END_DATE_CODE);
                }
                break;
        }
    }

    public void showDatePickerDialog(View v, int requestCode) {
        //here is your arguments
        Bundle bundle = new Bundle();
        bundle.putInt("requestCode", requestCode);
        Date initDateFormat = null;
        String date = initDate.getText().toString();
        if (date.equals("--/--/----")) {
            initDateFormat = new Date();
        } else {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            try {
                initDateFormat = formatter.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        bundle.putSerializable("initDate", initDateFormat);

        DialogFragment newFragment = new DatePickerFragmentDialog();
        newFragment.setArguments(bundle);
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    public OnFragmentInteractionListener getmListener() {
        return mListener;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void getFoodUpInfo(String name, int color, String initDate, String endDate, boolean checked);
    }
}
