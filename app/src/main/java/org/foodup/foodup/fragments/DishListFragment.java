package org.foodup.foodup.fragments;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.foodup.foodup.R;
import org.foodup.foodup.activities.DetailDishActivity;
import org.foodup.foodup.activities.EditDishActivity;
import org.foodup.foodup.adapters.DishesAdapter;
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.Dish;
import org.foodup.foodup.dialogs.BecomeUserDialog;
import org.foodup.foodup.interfaces.DishService;
import org.foodup.foodup.utils.ProgressDialogUtility;
import org.foodup.foodup.utils.ServiceGenerator;
import org.foodup.foodup.utils.SharedPreferencesManager;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Response;

/**
 * Users database where default dishes are displayed which aren't erasable
 * but editable when making a duplicate and dishes modified or created by
 * user which are both without needing to make a duplicate
 */
public class DishListFragment extends Fragment implements View.OnClickListener, DishesAdapter.OnDishInteractionListener {

    private static final int REQUEST_CODE_CREATE = 5;
    public ProgressDialog progressDialog;
    private EditText searchText;
    private DishesAdapter mAdapter;
    private int positionDish = -1;
    private Integer idMeal;

    public DishListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            positionDish = getArguments().getInt("positionDish");
            idMeal = getArguments().getInt("idMeal");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dish_list, container, false);

        searchText = (EditText) view.findViewById(R.id.search_dish);
        ImageButton clearTextButton = (ImageButton) view.findViewById(R.id.clear_text_button);
        FloatingActionButton createButton = (FloatingActionButton) view.findViewById(R.id.create_button);
        RecyclerView dishesList = (RecyclerView) view.findViewById(R.id.dishes_list);

        // use a linear layout manager
        dishesList.setLayoutManager(new LinearLayoutManager(getContext()));

        // specify an adapter (see also next example)
        mAdapter = new DishesAdapter(new ArrayList<Dish>(), this, idMeal, getActivity());
        dishesList.setAdapter(mAdapter);

        clearTextButton.setOnClickListener(this);
        createButton.setOnClickListener(this);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        return view;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.create_button:
                // send view to create dish
                if (SharedPreferencesManager.getIdUser() != 0) {
                    startActivityForResult(new Intent(getActivity(), EditDishActivity.class).putExtra("dish", new Dish()), REQUEST_CODE_CREATE);
                } else {
                    BecomeUserDialog dialog = new BecomeUserDialog();
                    dialog.show(getActivity().getSupportFragmentManager(), "dialog");
                }
                break;
            case R.id.clear_text_button:
                searchText.getText().clear();
                break;
        }
    }

    @Override
    public void startDishActivity(Dish dish, int action) {
        switch (action) {
            case DishesAdapter.VIEW_DISH:
                startActivity(
                        new Intent(getActivity(),
                                DetailDishActivity.class).
                                putExtra("dish", dish));
                break;
            case DishesAdapter.EDIT_DISH:
                startActivity(
                        new Intent(getActivity(),
                                EditDishActivity.class).
                                putExtra("duplicate", false).
                                putExtra("dish", dish));
                break;
            case DishesAdapter.DUPLICATE_DISH:
                startActivity(
                        new Intent(getActivity(),
                                EditDishActivity.class).
                                putExtra("duplicate", true).
                                putExtra("dish", dish));
                break;
        }
    }

    @Override
    public void getDish(Dish dish) {
        Log.d("dish", dish.toString());
        Log.d("position", positionDish + "");
        Log.d("idMeal", idMeal + "");
        getActivity().setResult(Activity.RESULT_OK, new Intent()
                .putExtra("position", positionDish)
                .putExtra("dish", dish)
                .putExtra("idMeal", idMeal));
        getActivity().finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        (new ServerTask()).execute();
    }

    class ServerTask extends AsyncTask<Void, Void, ArrayList<Dish>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialogUtility.getProgressDialog(getActivity());
            progressDialog.setCanceledOnTouchOutside(false);
            if (progressDialog != null) {
                progressDialog.setMessage(getString(R.string.loading_dishes));
                progressDialog.show();
            }
        }

        @Override
        protected ArrayList<Dish> doInBackground(Void... arg0) {

            try {
                Response<DataResponse<ArrayList<Dish>>> response;

                if (SharedPreferencesManager.getIdUser() != 0) {
                    DishService dishService = ServiceGenerator.createSecureService(DishService.class);
                    response = dishService.getDishes().execute();
                } else {
                    DishService dishService = ServiceGenerator.createService(DishService.class);
                    response = dishService.getDishesExplorer().execute();
                }

                Log.i(DishListFragment.class.getName(), "Code: " + response.code());
                Log.i(DishListFragment.class.getName(), "Message: " + response.message());

                if (response.isSuccess()) {
                    Log.i(DishListFragment.class.getName(), "DISHES: " + response.body().getData().size());
                    return response.body().getData();
                }
            } catch (IOException e) {
                Log.d(DishListFragment.class.getName(), "Error get user from server", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<Dish> result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null) {
                mAdapter.setDishes(result, searchText.getText().toString());
            } else {
                Snackbar snackbar = Snackbar
                        .make(getActivity().findViewById(android.R.id.content), getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                (new ServerTask()).execute();
                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        }
    }
}
