package org.foodup.foodup.fragments;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.foodup.foodup.R;
import org.foodup.foodup.adapters.UsersInCreateFoodUpAdapter;
import org.foodup.foodup.beans.User;
import org.foodup.foodup.utils.CurrentUser;
import org.foodup.foodup.utils.SharedPreferencesManager;

import java.util.ArrayList;


/**
 * Create foodUp second step which is adding people to it. It can be real user, searched by email,
 * or customized user configured by current user
 */
public class CreateFoodUpSecondFragment extends Fragment implements View.OnClickListener {

    private RecyclerView peopleList;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<User> users = new ArrayList<>();
    private FloatingActionButton addButton;

    private OnFragmentInteractionListener mListener;


    public CreateFoodUpSecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_food_up_second, container, false);

        //set list
        peopleList = (RecyclerView) view.findViewById(R.id.people);
        if (users.size() == 0) {
            if (SharedPreferencesManager.getToken() == null) {
                users.add(new User());
            } else {
                users.add(CurrentUser.get());
            }
        }

        //set button
        addButton = (FloatingActionButton) view.findViewById(R.id.add_people);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) addButton.getLayoutParams();
        params.rightMargin = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                getResources().getDimension(R.dimen.floatingButtonMargin),
                getResources().getDisplayMetrics()
        );
        addButton.setOnClickListener(this);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        peopleList.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new UsersInCreateFoodUpAdapter(users, getContext());
        peopleList.setAdapter(mAdapter);

        mListener.getUsers(users);
        // return inflated card
        return view;
    }

    /* Called when the second activity's finished */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:
                if (resultCode == getActivity().RESULT_OK) {
                    Bundle res = data.getExtras();
                    User configUser = (User) res.getSerializable("configUser");
                    int position = res.getInt("position");
                    users.set(position, configUser);
                    mAdapter.notifyItemChanged(position);
                    mListener.getUsers(users);
                }
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.getUsers(users);
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        users.add(new User());
        mAdapter.notifyDataSetChanged();
        mListener.getUsers(users);

        //move button if is needed
        if (mAdapter.getItemCount() > 1) {
            mListener.checkFloatingButtonPosition(peopleList, addButton);
        }
    }

    public void getUser(User user, int position) {
        users.set(position, user);
        mAdapter.notifyItemChanged(position);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        void getUsers(ArrayList<User> users);

        void checkFloatingButtonPosition(RecyclerView peopleList, FloatingActionButton addButton);

    }
}
