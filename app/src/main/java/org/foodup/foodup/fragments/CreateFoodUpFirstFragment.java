package org.foodup.foodup.fragments;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.foodup.foodup.R;

import java.util.ArrayList;

/**
 * Create foodUp first step which is choosing foodUp type:
 * mediterranean, gluten free, lactose free, vegetarian or vegan
 */
public class CreateFoodUpFirstFragment extends Fragment implements View.OnClickListener {

    private OnFragmentInteractionListener mListener;

    //variables
    private ArrayList<Button> buttons;
    //private int[] ids;

    public CreateFoodUpFirstFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_food_up_first, container, false);

       /* //upgrade:
       Bundle bundle = this.getArguments();
       ArrayList<FoodUp> foodups = (ArrayList<FoodUp>) bundle.getSerializable("foodups");
       final List<String> list = new ArrayList<>();
        list.add(getString(R.string.none));

        if (foodups.size() != 0) {
            final String[] foodUpNames = new String[foodups.size()];
            ids = new int[foodups.size()+1];
            ids[0] = -1;
            for (int i = 0; i < foodups.size(); i++) {
                foodUpNames[i] = foodups.get(i).getName();
                ids[(i + 1)] = foodups.get(i).getId();
            }
            list.addAll(Arrays.asList(foodUpNames));
        }

        //init and set spinner
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_expandable_list_item_1, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);*/

        //add buttons to ArrayList
        buttons = new ArrayList<>();
        buttons.add((Button) view.findViewById(R.id.mediterranean));
        buttons.add((Button) view.findViewById(R.id.fiber_rich));
        buttons.add((Button) view.findViewById(R.id.iron_rich));
        buttons.add((Button) view.findViewById(R.id.gluten_free));
        buttons.add((Button) view.findViewById(R.id.lactose_free));
        buttons.add((Button) view.findViewById(R.id.vegetarian));
        buttons.add((Button) view.findViewById(R.id.vegan));

        //set listeners
        for (Button button : buttons) {
            button.setOnClickListener(this);
        }

        mListener.getFoodUpType(-1);
        //return inflated card
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            mListener.getFoodUpType(getPressed());
        }
        for (Button button : buttons) {
            setPressed(button, false);
        }
        setPressed((Button) view, true);
    }

    public int getPressed() {
        for (int i = 0; i < buttons.size(); i++) {
            if (isPressed(buttons.get(i))) {
                return i;
            }
        }
        return -1;
    }

    public boolean isPressed(Button button) {
        return button.isPressed();
    }

    public void setPressed(Button button, boolean pressed) {
        if (pressed) {
            button.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            button.setPressed(true);

        } else {
            button.setBackgroundResource(android.R.drawable.btn_default);
            button.setPressed(false);
        }
    }

    /*//upgrade
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (ids != null) {
            mListener.getFollowingFoodUp(ids[i]);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void getFoodUpType(int buttonPressed);

        //upgrade:
        // void getFollowingFoodUp(int idFoodUp);
    }
}
