package org.foodup.foodup.fragments;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.foodup.foodup.R;
import org.foodup.foodup.activities.CreateFoodUpActivity;
import org.foodup.foodup.adapters.FoodUpsAdapter;
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.FoodUp;
import org.foodup.foodup.beans.Meal;
import org.foodup.foodup.interfaces.FoodUpService;
import org.foodup.foodup.utils.CurrentUser;
import org.foodup.foodup.utils.ProgressDialogUtility;
import org.foodup.foodup.utils.ServiceGenerator;
import org.foodup.foodup.utils.SharedPreferencesManager;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Response;

/**
 * Home fragment where user's foodUps are displayed
 */
public class FoodUpListFragment extends Fragment implements View.OnClickListener {

    private static final int REQUEST_CODE = 1;
    public ProgressDialog progressDialog;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<FoodUp> foodups = new ArrayList<>();
    private TextView emptyScreen;


    public FoodUpListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_food_up_list, container, false);
        FloatingActionButton createButton = (FloatingActionButton) view.findViewById(R.id.create_button);
        createButton.setOnClickListener(this);

        emptyScreen = (TextView) view.findViewById(R.id.emptyScreen);

        if (SharedPreferencesManager.getIdUser() != 0) {
            (new ServerTask()).execute();
        } else {
            if (CurrentUser.get().getFoodUps() == null) {
                CurrentUser.get().setFoodUps(new ArrayList<FoodUp>());
            } else {
                foodups = CurrentUser.get().getFoodUps();
                if (foodups.size() > 0) {
                    emptyScreen.setVisibility(View.GONE);
                } else {
                    emptyScreen.setVisibility(View.VISIBLE);
                }
            }
        }

        RecyclerView foodupsView = (RecyclerView) view.findViewById(R.id.foodups);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        foodupsView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new FoodUpsAdapter(foodups, getActivity());
        foodupsView.setAdapter(mAdapter);

        return view;
    }


    @Override
    public void onClick(View view) {
        startActivityForResult(new Intent(getActivity(), CreateFoodUpActivity.class).putExtra("foodups", foodups), REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == getActivity().RESULT_OK) {
                    Bundle res = data.getExtras();

                    FoodUp foodUp = (FoodUp) res.get("foodUp");

                    viewInfoResponse(foodUp);

                    if (SharedPreferencesManager.getIdUser() == 0) {
                        CurrentUser.get().getFoodUps().add(foodUp);
                    }

                    foodups.add(foodUp);
                    mAdapter.notifyDataSetChanged();
                    emptyScreen.setVisibility(View.GONE);
                }
                break;
        }
    }

    private void viewInfoResponse(FoodUp foodUp) {
        StringBuilder sb = new StringBuilder();
        for (Meal meal : foodUp.getMeals()) {
            long val = meal.getDay();
            Date date = new Date(val);
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
            String dateText = df.format(date);
            sb.append("\nDay meal: ").append(dateText).append(", Position of day: ").append(meal.getPositionOfDay());
        }
        Log.i("FoodUpListFragment", "Request FoodUp create, meals date:" + sb.toString());
    }

    /**
     * Get user's info from server
     */
    class ServerTask extends AsyncTask<Void, Void, ArrayList<FoodUp>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialogUtility.getProgressDialog(getActivity());
            progressDialog.setCanceledOnTouchOutside(false);
            if (progressDialog != null) {
                progressDialog.setMessage(getString(R.string.loading_foodups));
                progressDialog.show();
            }
        }

        @Override
        protected ArrayList<FoodUp> doInBackground(Void... arg0) {

            FoodUpService foodUpService = ServiceGenerator.createSecureService(FoodUpService.class);

            try {
                Response<DataResponse<ArrayList<FoodUp>>> response = foodUpService.getFoodUps().execute();

                Log.i(DishListFragment.class.getName(), "Code: " + response.code());
                Log.i(DishListFragment.class.getName(), "Message: " + response.message());

                if (response.isSuccess()) {
                    return response.body().getData();
                }
            } catch (IOException e) {
                Log.d(FoodUpListFragment.class.getName(), "Error get foodups from server", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<FoodUp> result) {

            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null) {
                foodups.clear();
                foodups.addAll(result);
                mAdapter.notifyDataSetChanged();

                if (foodups.size() > 0) {
                    emptyScreen.setVisibility(View.GONE);
                } else {
                    emptyScreen.setVisibility(View.VISIBLE);
                }
            } else {
                Snackbar snackbar = Snackbar
                        .make(getActivity().findViewById(android.R.id.content), getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                (new ServerTask()).execute();
                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        }
    }

}
