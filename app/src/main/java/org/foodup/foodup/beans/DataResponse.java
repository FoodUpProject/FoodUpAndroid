package org.foodup.foodup.beans;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 *Used to retrieve information from server
 * @param <T> object of response
 */
public class DataResponse<T> implements Serializable {

    @SerializedName("data")
    private T data;

    private String token;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}