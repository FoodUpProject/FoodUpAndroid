package org.foodup.foodup.beans;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.io.Serializable;

/**
 * Element used to create dishes
 */
public class Ingredient implements Serializable {

    private Aliment aliment;
    private Float quantity;
    private Integer unit;
    private Boolean mainIngredient;

    public Aliment getAliment() {
        return aliment;
    }

    public void setAliment(Aliment aliment) {
        this.aliment = aliment;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    public Boolean getMainIngredient() {
        return mainIngredient;
    }

    public void setMainIngredient(Boolean mainIngredient) {
        this.mainIngredient = mainIngredient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ingredient that = (Ingredient) o;

        return aliment.equals(that.aliment);

    }

    @Override
    public int hashCode() {
        return aliment.hashCode();
    }
}
