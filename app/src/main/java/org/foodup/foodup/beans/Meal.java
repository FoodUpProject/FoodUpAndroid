package org.foodup.foodup.beans;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.io.Serializable;
import java.util.List;

/**
 * Element used to create a menu
 */
public class Meal implements Serializable {

    private Integer id;
    private Long day;
    private Integer positionOfDay;
    private List<Dish> dishes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getDay() {
        return day;
    }

    public void setDay(Long day) {
        this.day = day;
    }

    public Integer getPositionOfDay() {
        return positionOfDay;
    }

    public void setPositionOfDay(Integer positionOfDay) {
        this.positionOfDay = positionOfDay;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }
}
