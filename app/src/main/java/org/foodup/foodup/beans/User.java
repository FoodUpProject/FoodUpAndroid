package org.foodup.foodup.beans;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {

    private Integer id;
    private String name;
    private String mail;
    private Integer quantity;
    private ArrayList<Aliment> allergies;
    private Integer intolerance;
    private ArrayList<User> coleguis;
    private ArrayList<FoodUp> foodUps;
    private Long lastUpdate;
    private String photo;
    private byte[] photoFile;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public ArrayList<Aliment> getAllergies() {
        return allergies;
    }

    public void setAllergies(ArrayList<Aliment> allergies) {
        this.allergies = allergies;
    }

    public Integer getIntolerance() {
        return intolerance;
    }

    public void setIntolerance(Integer intolerance) {
        this.intolerance = intolerance;
    }

    public ArrayList<User> getColeguis() {
        return coleguis;
    }

    public void setColeguis(ArrayList<User> coleguis) {
        this.coleguis = coleguis;
    }

    public ArrayList<FoodUp> getFoodUps() {
        return foodUps;
    }

    public void setFoodUps(ArrayList<FoodUp> foodUps) {
        this.foodUps = foodUps;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public byte[] getPhotoFile() {
        return photoFile;
    }

    public void setPhotoFile(byte[] photoFile) {
        this.photoFile = photoFile;
    }
}
