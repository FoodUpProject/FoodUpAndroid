package org.foodup.foodup.interfaces;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.Dish;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Interface to communicate android dishes with server dishes used by Retrofit
 */
public interface DishService {

    @GET("/resources/dishes")
    Call<DataResponse<ArrayList<Dish>>> getDishes();

    @GET("/resources/dishes/explorer")
    Call<DataResponse<ArrayList<Dish>>> getDishesExplorer();

    @POST("/resources/dishes")
    Call<DataResponse<Dish>> createDish(@Body Dish dish);

    @PUT("/resources/dishes")
    Call<DataResponse<Dish>> setDish(@Body Dish dish);

    @DELETE("/resources/dishes/{id}")
    Call<DataResponse<Boolean>> deleteDish(@Path("id") int idDish);

}
