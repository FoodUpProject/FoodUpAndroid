package org.foodup.foodup.interfaces;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.FoodUp;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Interface to communicate android foodUp's with server foodUp's used by Retrofit
 */
public interface FoodUpService {

    @GET("/resources/foodups")
    Call<DataResponse<ArrayList<FoodUp>>> getFoodUps();

    @GET("/resources/foodups/{id}")
    Call<DataResponse<FoodUp>> getFoodUp(@Path("id") int idFoodUp);

    @POST("/resources/foodups")
    Call<DataResponse<FoodUp>> setFoodUp(@Body FoodUp foodUp);

    @POST("/resources/foodups/explorer")
    Call<DataResponse<FoodUp>> setFoodUpExplorer(@Body FoodUp foodUp);

    @DELETE("/resources/foodups/{id}")
    Call<DataResponse<Boolean>> deleteFoodUp(@Path("id") int idFoodUp);

    @DELETE("/resources/foodups/meals/{id}/dishes/{id-dish}")
    Call<DataResponse<Boolean>> deleteMenuDish(@Path("id") int idMeal,@Path("id-dish") int idDish);

    @PUT("/resources/foodups/meals/{id}/dishes/{id-dish}/{new-id-dish}")
    Call<DataResponse<Boolean>> updateMenuDish(@Path("id") int idMeal, @Path("id-dish") int position, @Path("new-id-dish") int idDish);

}
