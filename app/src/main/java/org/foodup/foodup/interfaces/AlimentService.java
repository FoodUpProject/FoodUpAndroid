package org.foodup.foodup.interfaces;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
import org.foodup.foodup.beans.Aliment;
import org.foodup.foodup.beans.DataResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Interface to communicate android aliments with server aliments used by Retrofit
 */
public interface AlimentService {

    @GET("/resources/aliments")
    Call<DataResponse<ArrayList<Aliment>>> getAlimentsInfo();
}
