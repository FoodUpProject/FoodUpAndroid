package org.foodup.foodup.interfaces;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Interface to communicate android users with users dishes used by Retrofit
 */
public interface UserService {

    @GET("/resources/users")
    Call<DataResponse<User>> getUser();

    @POST("/resources/users")
    Call<DataResponse<User>> createUser();

    @PUT("/resources/users")
    Call<DataResponse<User>> setUser(@Body User user);

    @GET("/resources/users")
    Call<DataResponse<User>> getUserByMail(@Query("email") String mail);
}