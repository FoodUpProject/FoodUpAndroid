package org.foodup.foodup.adapters;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.foodup.foodup.R;
import org.foodup.foodup.beans.Ingredient;

import java.util.ArrayList;

/**
 * Sets data in ingredients list in order to make a dish
 */
public class IngredientsAdapter extends RecyclerView.Adapter<IngredientsAdapter.ViewHolder> {

    private ArrayList<Ingredient> ingredients;
    private boolean onlyReader;
    private String[] units;
    private int[] unitsValues;

    public IngredientsAdapter(ArrayList<Ingredient> ingredients, Context context) {
        this.ingredients = ingredients;
        onlyReader = false;
        units = context.getResources().getStringArray(R.array.unitsPositions);
        unitsValues = context.getResources().getIntArray(R.array.unitsPositionsValues);
    }

    public IngredientsAdapter(ArrayList<Ingredient> ingredients, Context context, boolean onlyReader) {
        this.ingredients = ingredients;
        this.onlyReader = onlyReader;
        units = context.getResources().getStringArray(R.array.unitsPositions);
        unitsValues = context.getResources().getIntArray(R.array.unitsPositionsValues);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new card
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ingredient, parent, false);
        // set the card's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        //declaring OnClickListener as an object
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {

                    case R.id.delete_button:

                        if (holder.deleteLayout.isShown()) {
                            //delete item
                            ingredients.remove(holder.getAdapterPosition());
                            notifyItemRemoved(holder.getAdapterPosition());
                            notifyItemRangeChanged(holder.getAdapterPosition(), getItemCount());

                            updateView(holder.mView, holder);
                        }
                        break;

                    default:

                        view = holder.mView;

                        if (updateView(view, holder) && !holder.deleteLayout.isShown()) {
                            holder.deleteLayout.setVisibility(View.GONE);
                            updateView(view, holder);
                        } else {
                            holder.deleteLayout.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        };

        if (!onlyReader) {
            //declaring OnLongClickListener as an object
            View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    view = holder.mView;
                    updateView(view, holder);
                    holder.deleteLayout.setVisibility(View.VISIBLE);
                    return true;
                }
            };

            //enable OnLongClickListener
            holder.mView.setLongClickable(true);
            holder.mView.setOnLongClickListener(longClickListener);
        }
        holder.deleteLayout.setClickable(true);

        //set OnClickListener
        holder.mView.setOnClickListener(listener);
        holder.deleteLayout.setOnClickListener(listener);
        holder.deleteButton.setOnClickListener(listener);

        Ingredient ingredient = ingredients.get(position);
        String infoIngredient = ingredient.getQuantity() + " " + getStringFromUnits(ingredient.getUnit()) + " - "
                + ingredient.getAliment().getName();
        holder.ingredient.setText(infoIngredient);

    }

    @Override
    public int getItemCount() {
        return ingredients.size();
    }

    /**
     * Hide possible bins
     */
    public boolean updateView(View v, ViewHolder holder) {

        for (int i = 0; i < getItemCount(); i++) {
            View otherView = ((RecyclerView) v.getParent()).getChildAt(i);

            if (i != holder.getLayoutPosition() && otherView != null) {
                RelativeLayout deleteLayout = (RelativeLayout) otherView.findViewById(R.id.deleteview);
                if (deleteLayout.isShown()) {
                    deleteLayout.setVisibility(View.GONE);
                    return true;
                }
            }

        }
        return false;
    }

    private String getStringFromUnits(int unit) {
        for (int i = 0; i < unitsValues.length; i++) {
            if (unitsValues[i] == unit) {
                return units[i];
            }
        }
        return null;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one card per item, and
    // you provide access to all the views for a data item in a card holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        // each data item is just a string in this case
        public TextView ingredient;

        public RelativeLayout deleteLayout;
        public ImageView deleteButton;


        public ViewHolder(View v) {
            super(v);
            mView = v;

            ingredient = (TextView) v.findViewById(R.id.ingredient_name);
            deleteLayout = (RelativeLayout) v.findViewById(R.id.deleteview);
            deleteButton = (ImageView) v.findViewById(R.id.delete_button);

        }

    }
}
