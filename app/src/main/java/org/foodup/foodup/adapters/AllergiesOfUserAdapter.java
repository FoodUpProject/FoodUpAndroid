package org.foodup.foodup.adapters;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.foodup.foodup.R;
import org.foodup.foodup.beans.Aliment;

import java.util.ArrayList;

/**
 * Sets user allergies in config user activity
 */
public class AllergiesOfUserAdapter extends RecyclerView.Adapter<AllergiesOfUserAdapter.ViewHolder> {

    private ArrayList<Aliment> userAllergies;

    // Provide a suitable constructor (depends on the kind of dataset)
    public AllergiesOfUserAdapter(ArrayList<Aliment> userAllergies) {
        this.userAllergies = userAllergies;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new card
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_aliment, parent, false);
        // set the card's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    // Replace the contents of a card (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the card with that element

        //declaring OnClickListener as an object
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {

                    case R.id.delete_button:

                        if (holder.deleteLayout.isShown()) {

                            //delete item
                            int position = (Integer) view.getTag();
                            userAllergies.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, getItemCount());

                            updateView(holder.mView, holder);

                        }

                        break;

                    default:

                        view = holder.mView;

                        if (updateView(view, holder) || holder.deleteLayout.isShown()) {
                            holder.deleteLayout.setVisibility(View.GONE);
                        }

                        break;
                }
            }
        };

        //set listeners
        holder.mView.setOnClickListener(listener);
        holder.name.setOnClickListener(listener);

        //set delete listener
        holder.deleteButton.setTag(position);
        holder.deleteButton.setOnClickListener(listener);

        //enable OnLongClickListener
        holder.mView.setLongClickable(true);
        holder.name.setLongClickable(true);


        //declaring OnLongClickListener as an object
        View.OnLongClickListener longListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                view = holder.mView;
                updateView(view, holder);
                holder.deleteLayout.setVisibility(View.VISIBLE);
                return true;
            }
        };

        //set onLongClickListener
        holder.mView.setOnLongClickListener(longListener);
        holder.name.setOnLongClickListener(longListener);

        holder.deleteLayout.setVisibility(View.GONE);

        //set data
        holder.name.setText(userAllergies.get(position).getName());

    }

    /**
     * Return the size of your dataset (invoked by the layout manager)
     */
    @Override
    public int getItemCount() {
        return userAllergies.size();
    }

    /**
     * hides possible bins
     */
    public boolean updateView(View v, ViewHolder holder) {

        for (int i = 0; i < getItemCount(); i++) {
            View otherView = ((RecyclerView) v.getParent()).getChildAt(i);

            if (i != holder.getLayoutPosition() && otherView != null) {
                RelativeLayout deleteLayout = (RelativeLayout) otherView.findViewById(R.id.deleteview);
                if (deleteLayout.isShown()) {
                    deleteLayout.setVisibility(View.GONE);
                    return true;
                }
            }

        }
        return false;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one card per item, and
    // you provide access to all the views for a data item in a card holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        // each data item is just a string in this case
        public TextView name;

        public RelativeLayout mainLayout;
        public RelativeLayout deleteLayout;
        public ImageView deleteButton;

        public ViewHolder(View v) {

            super(v);

            mView = v;
            name = (TextView) v.findViewById(R.id.aliment_name);

            mainLayout = (RelativeLayout) v.findViewById(R.id.mainview);
            //layout displayed onLongClick
            deleteLayout = (RelativeLayout) v.findViewById(R.id.deleteview);
            //button to click when background layout is displayed
            deleteButton = (ImageView) v.findViewById(R.id.delete_button);
        }

    }
}
