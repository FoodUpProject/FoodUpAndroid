package org.foodup.foodup.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.foodup.foodup.R;
import org.foodup.foodup.beans.Meal;
import org.foodup.foodup.utils.Commons;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    private ArrayList<String> dates;
    private List<Meal> meals;
    private Context context;

    //upgrade:
    //private boolean isMeals=true;

    public MenuAdapter(LinkedHashSet<String> dates, List<Meal> meals, Context context) {
        this.dates = new ArrayList<>();
        this.dates.addAll(dates);
        this.meals = meals;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new card
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_menu_day, parent, false);
        // set the card's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final List<Meal> properMeals = new ArrayList<>();

        holder.date.setText(dates.get(position));

        //get meals which day matches date in holder position
        int count = 0;
        for (Meal meal : meals) {
            if (Commons.longToString(meal.getDay()).equals(dates.get(position))) {
                properMeals.add(meal);
                count++;
                if (count == 3) {
                    break;
                }
            }
        }

        //init meals
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.meals.setLayoutManager(layoutManager);
        MenuMealAdapter menuAdapter = new MenuMealAdapter(properMeals, context);
        holder.meals.setAdapter(menuAdapter);
    }



    @Override
    public int getItemCount() {
        return dates.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one card per item, and
    // you provide access to all the views for a data item in a card holder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;

        // each data item is just a string in this case
        public TextView date;
        public RecyclerView meals;


        public ViewHolder(View v) {
            super(v);
            mView = v;
            date = (TextView) v.findViewById(R.id.date_text);
            meals = (RecyclerView) v.findViewById(R.id.dishes_or_meals);
        }


    }
}
