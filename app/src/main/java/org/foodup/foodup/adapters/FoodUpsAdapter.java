package org.foodup.foodup.adapters;

/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.foodup.foodup.R;
import org.foodup.foodup.activities.MenuFoodUpActivity;
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.FoodUp;
import org.foodup.foodup.interfaces.FoodUpService;
import org.foodup.foodup.utils.Commons;
import org.foodup.foodup.utils.ProgressDialogUtility;
import org.foodup.foodup.utils.ServiceGenerator;
import org.foodup.foodup.utils.SharedPreferencesManager;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Callback;
import retrofit2.Response;

/**
 * Sets data in foodups list fragment (home)
 */
public class FoodUpsAdapter extends RecyclerView.Adapter<FoodUpsAdapter.ViewHolder> {

    private ArrayList<FoodUp> foodups;
    private Context context;
    private ProgressDialog progressDialog;

    // Provide a suitable constructor (depends on the kind of dataset)
    public FoodUpsAdapter(ArrayList<FoodUp> myDataSet, Context context) {
        foodups = myDataSet;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FoodUpsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new card
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_foodup, parent, false);
        // set the card's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {// - get element from your dataset at this position
        // - replace the contents of the card with that element

        //declaring OnClickListener as an object
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {

                    case R.id.delete_foodup_button:

                        if (holder.optionsLayout.isShown()) {
                            if (SharedPreferencesManager.getIdUser() != 0) {
                                deleteFoodUp(holder, holder.getAdapterPosition());
                            } else {
                                foodups.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, getItemCount());
                            }
                        }

                        break;

                 /*   case R.id.edit_foodup_button:

                        if (holder.optionsLayout.isShown()) {
                            ((Activity) context).startActivityForResult(
                                    new Intent(context,
                                            EditFoodUpActivity.class).
                                            putExtra("foodUp", foodups.get(position)),
                                    REQUEST_CODE);
                            holder.optionsLayout.setVisibility(View.GONE);
                        }

                        break;*/
                    case R.id.card_view:
                        view = holder.mView;
                        if (!updateView(view, holder) && !holder.optionsLayout.isShown()) {
                            if (SharedPreferencesManager.getIdUser() != 0) {
                                (new ServerTask()).execute(foodups.get(holder.getAdapterPosition()).getId());
                            } else {
                                context.startActivity(new Intent(context, MenuFoodUpActivity.class)
                                        .putExtra("foodup", foodups.get(holder.getAdapterPosition())));
                            }
                        } else {
                            holder.optionsLayout.setVisibility(View.GONE);
                        }
                        break;

                    default:
                        view = holder.mView;
                        if (!updateView(view, holder) && !holder.optionsLayout.isShown()) {
                            if (SharedPreferencesManager.getIdUser() != 0) {
                                (new ServerTask()).execute(foodups.get(holder.getAdapterPosition()).getId());
                            } else {
                                context.startActivity(new Intent(context, MenuFoodUpActivity.class)
                                        .putExtra("foodup", foodups.get(holder.getAdapterPosition())));
                            }
                        } else {
                            holder.optionsLayout.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        };

        //set listeners
        holder.card.setOnClickListener(listener);
        //set delete listener
        holder.deleteButton.setOnClickListener(listener);

        //enable OnLongClickListener
        holder.card.setLongClickable(true);

        //declaring OnLongClickListener as an object
        View.OnLongClickListener longListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                view = holder.mView;
                updateView(view, holder);
                holder.optionsLayout.setVisibility(View.VISIBLE);
                return true;
            }
        };

        //set onLongClickListener
        holder.card.setOnLongClickListener(longListener);

        holder.optionsLayout.setVisibility(View.GONE);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        holder.users.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        UsersInCardAdapter mAdapter = new UsersInCardAdapter(
                foodups.get(position).getDiners());
        holder.users.setAdapter(mAdapter);

        holder.card.setCardBackgroundColor(Commons.getColor(foodups.get(position).getColor(), context));
        holder.mTextView.setText(foodups.get(position).getName());

      /* //future implementations (upgrade):
        Integer followingId = foodups.get(position).getFollowingFoodUp();

        if (followingId != null) {
            for (int i = 0; i < getItemCount(); i++) {
                if (foodups.get(i).getId().equals(followingId)) {
                    holder.followingFoodUp.setVisibility(View.VISIBLE);
                    holder.followingFoodUp.setText(context.getString(R.string.based_in) + " " + foodups.get(i).getName());
                }
            }
        }*/


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return foodups.size();
    }

    //hide possible bins
    public boolean updateView(View v, ViewHolder holder) {

        for (int i = 0; i < getItemCount(); i++) {
            View otherView = ((RecyclerView) v.getParent()).getChildAt(i);

            if (i != holder.getLayoutPosition() && otherView != null) {
                CardView deleteLayout = (CardView) otherView.findViewById(R.id.options_foodup);
                if (deleteLayout.isShown()) {
                    deleteLayout.setVisibility(View.GONE);
                    return true;
                }
            }

        }
        return false;
    }

    private void deleteFoodUp(final ViewHolder holder, final int position) {
        FoodUpService foodUpService = ServiceGenerator.createSecureService(FoodUpService.class);
        foodUpService.deleteFoodUp(foodups.get(position).getId()).enqueue(new Callback<DataResponse<Boolean>>() {
            @Override
            public void onResponse(Response<DataResponse<Boolean>> response) {
                if (response.isSuccess()) {
                    if (response.body().getData()) {
                        //delete item
                        foodups.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, getItemCount());

                        updateView(holder.mView, holder);
                    } else {
                        Log.i("FoodUpsAdapter", "No se ha podido eliminar el FoodUp con id " + foodups.get(position).getId());
                    }
                }
                Log.i("FoodUpsAdapter", "Code: " + response.code());
                Log.i("FoodUpsAdapter", "Message: " + response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                Log.i("FoodUpsAdapter", t.getMessage(), t);
                Snackbar snackbar = Snackbar
                        .make(((Activity) context).findViewById(android.R.id.content), context.getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                        .setAction(context.getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                deleteFoodUp(holder, position);
                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        });
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one card per item, and
    // you provide access to all the views for a data item in a card holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        // each data item is just a string in this case
        public TextView mTextView;
        public TextView followingFoodUp;
        public CardView card;
        public RecyclerView users;

        public CardView optionsLayout;
        public ImageView deleteButton;
        //public ImageView editButton;


        public ViewHolder(View v) {
            super(v);
            mView = v;
            card = (CardView) v.findViewById(R.id.card_view);
            mTextView = (TextView) v.findViewById(R.id.foodup_name);
            followingFoodUp = (TextView) v.findViewById(R.id.following);
            users = (RecyclerView) v.findViewById(R.id.users);

            //layout displayed onLongClick
            optionsLayout = (CardView) v.findViewById(R.id.options_foodup);
            //buttons to click when background layout is displayed
            deleteButton = (ImageView) v.findViewById(R.id.delete_foodup_button);
            //editButton = (ImageView) v.findViewById(R.id.edit_foodup_button);
        }

    }

    class ServerTask extends AsyncTask<Integer, Void, FoodUp> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialogUtility.getProgressDialog(context);
            progressDialog.setCanceledOnTouchOutside(false);
            if (progressDialog != null) {
                progressDialog.setMessage("Recibiendo datos de los platos, por favor, espere...");
                progressDialog.show();
            }
        }

        @Override
        protected FoodUp doInBackground(Integer... idFoodUp) {

            FoodUpService dishService = ServiceGenerator.createSecureService(FoodUpService.class);

            try {
                Response<DataResponse<FoodUp>> response = dishService.getFoodUp(idFoodUp[0]).execute();

                Log.i(FoodUpsAdapter.class.getName(), "Code: " + response.code());
                Log.i(FoodUpsAdapter.class.getName(), "Message: " + response.message());

                if (response.isSuccess()) {
                    return response.body().getData();
                }
            } catch (IOException e) {
                Log.d(FoodUpsAdapter.class.getName(), "Error get foodUp from server", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(FoodUp result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null) {
                context.startActivity(new Intent(context, MenuFoodUpActivity.class)
                        .putExtra("foodup", result));
            } /*else {
                Snackbar snackbar = Snackbar
                        .make(getActivity().findViewById(android.R.id.content), getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                (new ServerTask()).execute();
                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }*/
        }
    }

}
