package org.foodup.foodup.adapters;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.foodup.foodup.R;
import org.foodup.foodup.beans.User;
import org.foodup.foodup.utils.CircleTransform;
import org.foodup.foodup.utils.ServiceGenerator;

import java.util.ArrayList;

/**
 * Sets users in foodUp card on FoodUp's list (home fragment)
 */
public class UsersInCardAdapter extends RecyclerView.Adapter<UsersInCardAdapter.ViewHolder> {

    private ArrayList<User> users;

    public UsersInCardAdapter(ArrayList<User> users) {
        this.users = users;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new card
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_foodup_users_in_card, parent, false);
        // set the card's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    // Replace the contents of a card (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        User user = users.get(position);
        //set data
        if (user.getId() != null && user.getId() > 0) {  //if user is real
            //set photo
            Picasso.with(holder.mView.getContext()).load(ServiceGenerator.PROFILE_PHOTO_URL + user.getId()).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE)
                    .placeholder(holder.mView.getResources().getDrawable(R.drawable.ic_user_circle_black))
                    .error(holder.mView.getResources().getDrawable(R.drawable.ic_user_circle_black))
                    .resize(50, 50)
                    .centerCrop().transform(new CircleTransform()).into(holder.picture);
        } else {
            String photoPath = user.getPhoto();
            if (photoPath != null) {
                Picasso.with(holder.mView.getContext()).load(Uri.parse(photoPath)).memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).resize(50, 50)
                        .centerCrop().transform(new CircleTransform()).into(holder.picture);
            }
        }
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return users.size();
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one card per item, and
    // you provide access to all the views for a data item in a card holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        // each data item is just a string in this case
        public ImageView picture;


        public ViewHolder(View v) {
            super(v);
            mView = v;

            picture = (ImageView) v.findViewById(R.id.image_user);

        }

    }
}
