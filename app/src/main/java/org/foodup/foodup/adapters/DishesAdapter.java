package org.foodup.foodup.adapters;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This is free software, licensed under the GNU General Public License v3.
 *         See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.foodup.foodup.R;
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.Dish;
import org.foodup.foodup.dialogs.BecomeUserDialog;
import org.foodup.foodup.interfaces.DishService;
import org.foodup.foodup.utils.ProgressDialogUtility;
import org.foodup.foodup.utils.ServiceGenerator;
import org.foodup.foodup.utils.SharedPreferencesManager;

import java.util.ArrayList;

import retrofit2.Callback;
import retrofit2.Response;

/**
 * Sets data in dishes database list fragment
 */
public class DishesAdapter extends RecyclerView.Adapter<DishesAdapter.ViewHolder> implements Filterable {

    public static final int VIEW_DISH = 1;
    public static final int EDIT_DISH = 2;
    public static final int DUPLICATE_DISH = 3;

    protected ProgressDialog progressDialog;
    private ArrayList<Dish> dishes;
    private ArrayList<Dish> filteredDishes;
    private OnDishInteractionListener mListener;
    private Context context;
    private Integer idMeal;

    public DishesAdapter(ArrayList<Dish> dishes, OnDishInteractionListener mListener, Integer idMeal, Context context) {
        this.dishes = dishes;
        this.mListener = mListener;
        filteredDishes = new ArrayList<>(dishes);
        this.idMeal = idMeal;
        this.context = context;

        progressDialog = ProgressDialogUtility.getProgressDialog(context);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    public void setDishes(ArrayList<Dish> dishes, String textFilter) {
        this.dishes = dishes;
        getFilter().filter(textFilter);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new card
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dishes, parent, false);
        // set the card's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    // Replace the contents of a card (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final boolean isExplorer = SharedPreferencesManager.getIdUser() != 0;

        //declaring OnClickListener as an object
        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                switch (view.getId()) {

                    case R.id.delete_button:
                        deleteDish(holder, holder.getAdapterPosition());
                        break;

                    case R.id.edit_button:
                        mListener.startDishActivity(filteredDishes.get(holder.getAdapterPosition()), EDIT_DISH);
                        holder.optionsLayout.setVisibility(View.GONE);
                        break;

                    case R.id.duplicate_button:
                        if (isExplorer) {
                            mListener.startDishActivity(filteredDishes.get(holder.getAdapterPosition()), DUPLICATE_DISH);
                        } else {
                            BecomeUserDialog dialog = new BecomeUserDialog();
                            dialog.show(((AppCompatActivity) context).getSupportFragmentManager(), "dialog");
                        }
                        holder.optionsLayout.setVisibility(View.GONE);
                        break;

                    default:
                        if (idMeal == null) {
                            view = holder.mView;
                            if (!updateView(view, holder) && !holder.optionsLayout.isShown()) {
                                mListener.startDishActivity(filteredDishes.get(holder.getAdapterPosition()), VIEW_DISH);
                                holder.optionsLayout.setVisibility(View.GONE);
                            } else {
                                holder.optionsLayout.setVisibility(View.GONE);
                            }
                        } else {
                            mListener.getDish(filteredDishes.get(holder.getAdapterPosition()));
                        }
                        break;
                }
            }
        };

        //set listeners
        holder.card.setOnClickListener(listener);
        //set delete listener
        holder.deleteButton.setOnClickListener(listener);
        holder.editButton.setOnClickListener(listener);
        holder.duplicateButton.setOnClickListener(listener);
        holder.picture.setOnClickListener(listener);

        //enable OnLongClickListener
        holder.card.setLongClickable(true);

        //declaring OnLongClickListener as an object
        View.OnLongClickListener longListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (idMeal == null) {
                    view = holder.mView;
                    updateView(view, holder);
                    holder.optionsLayout.setVisibility(View.VISIBLE);
                    return true;
                }
                return false;
            }
        };

        //set onLongClickListener
        holder.card.setOnLongClickListener(longListener);

        holder.optionsLayout.setVisibility(View.GONE);

        Dish dish = filteredDishes.get(position);

        holder.name.setText(dish.getName());

        Picasso.with(holder.mView.getContext()).load(Uri.parse(ServiceGenerator.DISHES_URL + dish.getIdPicture()))
                .placeholder(holder.mView.getContext().getResources().getDrawable(R.drawable.ic_menu_dish))
                .error(holder.mView.getContext().getResources().getDrawable(R.drawable.ic_menu_dish))
                .resize(50, 50)
                .centerCrop().into(holder.picture);

        if (dish.getIdUserOwner() == SharedPreferencesManager.getIdUser()) {
            holder.deleteButton.setVisibility(View.VISIBLE);
            holder.editButton.setVisibility(View.VISIBLE);
            holder.duplicateButton.setVisibility(View.GONE);
        } else {
            holder.deleteButton.setVisibility(View.GONE);
            holder.editButton.setVisibility(View.GONE);
            holder.duplicateButton.setVisibility(View.VISIBLE);
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filteredDishes.size();
    }


    //hide possible bins
    public boolean updateView(View v, ViewHolder holder) {

        for (int i = 0; i < getItemCount(); i++) {
            View otherView = ((RecyclerView) v.getParent()).getChildAt(i);

            if (i != holder.getLayoutPosition() && otherView != null) {
                RelativeLayout deleteLayout = (RelativeLayout) otherView.findViewById(R.id.options);
                if (deleteLayout.isShown()) {
                    deleteLayout.setVisibility(View.GONE);
                    return true;
                }
            }

        }
        return false;
    }

    @Override
    public Filter getFilter() {
        return new DishFilter(this, dishes);
    }

    private void deleteDish(final ViewHolder holder, final int position) {

        progressDialog.setMessage("Eliminando el plato, por favor espere...");
        progressDialog.show();

        DishService dishService = ServiceGenerator.createSecureService(DishService.class);
        dishService.deleteDish(filteredDishes.get(position).getId()).enqueue(new Callback<DataResponse<Boolean>>() {
            @Override
            public void onResponse(Response<DataResponse<Boolean>> response) {
                progressDialog.dismiss();
                if (response.isSuccess()) {
                    if (response.body().getData()) {
                        dishes.remove(filteredDishes.get(position));
                        filteredDishes.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, getItemCount());

                        updateView(holder.mView, holder);
                    } else {
                        Log.i(DishesAdapter.class.getName(), "No se ha podido eliminar el Dish con id " + dishes.get(position).getId());
                    }
                }
                Log.i(DishesAdapter.class.getName(), "Code: " + response.code());
                Log.i(DishesAdapter.class.getName(), "Message: " + response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
                Log.i(DishesAdapter.class.getName(), t.getMessage(), t);
            }
        });
    }

    public interface OnDishInteractionListener {
        void startDishActivity(Dish dish, int accion);

        void getDish(Dish dish);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one card per item, and
    // you provide access to all the views for a data item in a card holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        // each data item is just a string in this case
        public ImageView picture;
        public TextView name;
        public CardView card;

        public RelativeLayout optionsLayout;
        public ImageView deleteButton;
        public ImageView editButton;
        public ImageView duplicateButton;


        public ViewHolder(View v) {
            super(v);
            mView = v;
            card = (CardView) v.findViewById(R.id.card_view);
            picture = (ImageView) v.findViewById(R.id.image_dish);
            name = (TextView) v.findViewById(R.id.dish_name);

            optionsLayout = (RelativeLayout) v.findViewById(R.id.options);
            deleteButton = (ImageView) v.findViewById(R.id.delete_button);
            editButton = (ImageView) v.findViewById(R.id.edit_button);
            duplicateButton = (ImageView) v.findViewById(R.id.duplicate_button);
        }

    }

    private static class DishFilter extends Filter {

        private final DishesAdapter adapter;

        private final ArrayList<Dish> originalList;

        private final ArrayList<Dish> filteredList;

        private DishFilter(DishesAdapter adapter, ArrayList<Dish> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = originalList;
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                filteredList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (Dish dish : originalList) {
                    if (dish.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(dish);
                    }
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filteredDishes.clear();
            adapter.filteredDishes.addAll((ArrayList<Dish>) results.values);
            adapter.notifyDataSetChanged();
            Log.i(DishesAdapter.class.getName(), "Filter results: " + ((ArrayList<Dish>) results.values).size());
        }
    }
}
