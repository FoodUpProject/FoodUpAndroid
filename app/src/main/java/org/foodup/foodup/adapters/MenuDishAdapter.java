package org.foodup.foodup.adapters;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 *         <p/>
 *         This  *         See http://www.gnu.org/licenses/gpl.html for more information.
is free software, licensed under the GNU General Public License v3.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.foodup.foodup.R;
import org.foodup.foodup.activities.DetailDishActivity;
import org.foodup.foodup.activities.ReplaceDishActivity;
import org.foodup.foodup.beans.DataResponse;
import org.foodup.foodup.beans.Dish;
import org.foodup.foodup.interfaces.FoodUpService;
import org.foodup.foodup.utils.ServiceGenerator;
import org.foodup.foodup.utils.SharedPreferencesManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

/**
 * Sets dishes of menu data in cards when showing the menu
 */
public class MenuDishAdapter extends RecyclerView.Adapter<MenuDishAdapter.ViewHolder> {

    private static final int REQUEST_CODE = 19;
    private final ArrayList<Dish> mValues;
    private final Context context;
    private Integer idMeal;

    public MenuDishAdapter(List<Dish> items, Context context, Integer id) {
        mValues = (ArrayList<Dish>) items;
        this.context = context;
        this.idMeal = id;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_menu_dish, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.title.setText(holder.mItem.getName());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.optionsLayout.isShown()) {
                    holder.optionsLayout.setVisibility(View.GONE);
                } else {
                    context.startActivity(new Intent(context, DetailDishActivity.class).putExtra("dish", holder.mItem));
                }
            }
        });

        //enable OnLongClickListener
        holder.mView.setLongClickable(true);

        //declaring OnLongClickListener as an object
        View.OnLongClickListener longListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                holder.optionsLayout.setVisibility(View.VISIBLE);
                return true;
            }
        };

        //set onLongClickListener
        holder.mView.setOnLongClickListener(longListener);

        //aks server to delete dish from server if it's a real user or from memory if it's explorer
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharedPreferencesManager.getIdUser() != 0) {
                    deleteDishOfMenu(holder, holder.getAdapterPosition(), idMeal);
                } else {
                    mValues.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                    notifyItemRangeChanged(holder.getAdapterPosition(), getItemCount());
                }
            }
        });

        holder.replace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) context).startActivityForResult(
                        new Intent(context, ReplaceDishActivity.class)
                                .putExtra("positionDish", holder.getAdapterPosition())
                                .putExtra("idMeal", idMeal),
                        REQUEST_CODE);
                holder.optionsLayout.setVisibility(View.GONE);
            }
        });

    }

    /**
     * Deletes dish from server
     */
    private void deleteDishOfMenu(final ViewHolder holder, final int position, final Integer idMeal) {
        FoodUpService foodupService = ServiceGenerator.createSecureService(FoodUpService.class);
        foodupService.deleteMenuDish(idMeal, mValues.get(position).getId()).enqueue(new Callback<DataResponse<Boolean>>() {
            @Override
            public void onResponse(Response<DataResponse<Boolean>> response) {
                if (response.isSuccess()) {
                    if (response.body().getData()) {
                        mValues.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, getItemCount());
                    } else {
                        Log.i(DishesAdapter.class.getName(), "No se ha podido eliminar el dish ");
                    }
                }
                Log.i(DishesAdapter.class.getName(), "Code: " + response.code());
                Log.i(DishesAdapter.class.getName(), "Message: " + response.message());
            }

            @Override
            public void onFailure(Throwable t) {
                Log.i(DishesAdapter.class.getName(), t.getMessage(), t);
                Snackbar snackbar = Snackbar
                        .make(((Activity) context).findViewById(android.R.id.content), context.getString(R.string.no_connection), Snackbar.LENGTH_LONG)
                        .setAction(context.getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                deleteDishOfMenu(holder, position, idMeal);
                            }
                        });

                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();

            }
        });
    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public TextView title;
        public Dish mItem;
        public CardView dish;
        public CardView optionsLayout;
        public ImageView delete;
        public ImageView replace;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            dish = (CardView) view.findViewById(R.id.dish);
            title = (TextView) view.findViewById(R.id.dish_title);
            optionsLayout = (CardView) view.findViewById(R.id.options_dish);
            delete = (ImageView) view.findViewById(R.id.delete_dish_button);
            replace = (ImageView) view.findViewById(R.id.replace_dish_button);
        }
    }
}
