package org.foodup.foodup.adapters;

/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.foodup.foodup.R;
import org.foodup.foodup.beans.Dish;
import org.foodup.foodup.beans.Meal;

import java.util.ArrayList;
import java.util.List;

/**
 * Sets meals data in menu when clicking on foodup of foodup's list fragment (home)
 */
public class MenuMealAdapter extends RecyclerView.Adapter<MenuMealAdapter.ViewHolder> {

    private final ArrayList<Meal> mValues;
    private final Context context;

    public MenuMealAdapter(List<Meal> items, Context context) {
        mValues = (ArrayList<Meal>) items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_meal, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        List<Dish> dishes = holder.mItem.getDishes();

        //init dishes
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        holder.mView.setLayoutManager(layoutManager);
        MenuDishAdapter menuDishAdapter = new MenuDishAdapter(dishes, context, mValues.get(position).getId());
        holder.mView.setAdapter(menuDishAdapter);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RecyclerView mView;
        public Meal mItem;

        public ViewHolder(View view) {
            super(view);
            mView = (RecyclerView) view;
        }
    }
}
