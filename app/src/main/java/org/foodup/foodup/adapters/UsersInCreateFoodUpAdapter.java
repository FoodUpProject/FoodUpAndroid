package org.foodup.foodup.adapters;
/**
 * @author Cristóbal Cabezas Mateos <ccabezasmateos@gmail.com>
 * @author Meritxell Martí Martínez <m.marblu@gmail.com>
 * @author Escola del treball <correu@escoladeltreball.org>
 * <p/>
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.foodup.foodup.R;
import org.foodup.foodup.activities.ConfigUserActivity;
import org.foodup.foodup.activities.CreateFoodUpActivity;
import org.foodup.foodup.beans.User;
import org.foodup.foodup.dialogs.AddUserDialog;
import org.foodup.foodup.dialogs.AssignUserExplorerDialog;
import org.foodup.foodup.utils.CircleTransform;
import org.foodup.foodup.utils.ServiceGenerator;
import org.foodup.foodup.utils.SharedPreferencesManager;

import java.util.ArrayList;

/**
 * Displays user's photos in foodUp card placed in foodUp's list fragment
 */
public class UsersInCreateFoodUpAdapter extends RecyclerView.Adapter<UsersInCreateFoodUpAdapter.ViewHolder> {

    private static final int REQUEST_CODE = 0;
    private ArrayList<User> users;
    private Context context;

    // Provide a suitable constructor (depends on the kind of dataset)
    public UsersInCreateFoodUpAdapter(ArrayList<User> users, Context context) {
        this.users = users;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new card
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_create_foodup_user, parent, false);
        // set the card's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    // Replace the contents of a card (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the card with that element

        //declaring OnClickListener as an object
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {

                    case R.id.delete_button:

                        if (holder.deleteLayout.isShown()) {

                            //delete item
                            int position = (Integer) view.getTag();
                            users.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, getItemCount());
                            ((CreateFoodUpActivity) context).getUsers(users);

                            //move add button if needed
                            if (context instanceof CreateFoodUpActivity) {
                                RecyclerView list = (RecyclerView) ((CreateFoodUpActivity) context).findViewById(R.id.people);
                                FloatingActionButton addButton = (FloatingActionButton) ((CreateFoodUpActivity) context).findViewById(R.id.add_people);
                                ((CreateFoodUpActivity) context).checkFloatingButtonPosition(list, addButton);
                            }

                            updateView(holder.mView, holder);

                        }

                        break;

              /*      //upgrade
                        case R.id.add_dishes:

                        if (holder.deleteLayout.isShown()) {
                            holder.deleteLayout.setVisibility(View.GONE);
                        } else if (!updateView(holder.mView, holder)) {
                        }

                        break;*/

                    case R.id.settings:

                        if (holder.deleteLayout.isShown()) {
                            holder.deleteLayout.setVisibility(View.GONE);
                        } else if (!updateView(holder.mView, holder)) {
                            ((Activity) context).startActivityForResult(
                                    new Intent(context,
                                            ConfigUserActivity.class).
                                            putExtra("position", holder.getAdapterPosition()).putExtra("configUser",
                                            users.get(holder.getAdapterPosition())),
                                    REQUEST_CODE);
                        }

                        break;

                    default:

                        view = holder.mView;

                        if (!updateView(view, holder) && !holder.deleteLayout.isShown()) {
                            showNoticeDialog(holder.getAdapterPosition());

                        } else {
                            holder.deleteLayout.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        };

        //set listeners
        holder.mView.setOnClickListener(listener);
        holder.name.setOnClickListener(listener);
        holder.remember.setOnClickListener(listener);
        holder.picture.setOnClickListener(listener);
        holder.settingsButton.setOnClickListener(listener);

        //set delete listener
        holder.deleteButton.setTag(position);
        holder.deleteButton.setOnClickListener(listener);

        //enable OnLongClickListener
        holder.mView.setLongClickable(true);
        holder.name.setLongClickable(true);
        holder.remember.setLongClickable(true);
        holder.picture.setLongClickable(true);
        holder.settingsButton.setLongClickable(true);

        //declaring OnLongClickListener as an object
        View.OnLongClickListener longListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                view = holder.mView;
                updateView(view, holder);
                holder.deleteLayout.setVisibility(View.VISIBLE);
                return true;
            }
        };

        //set onLongClickListener
        holder.mView.setOnLongClickListener(longListener);
        holder.name.setOnLongClickListener(longListener);
        holder.remember.setOnLongClickListener(longListener);
        holder.picture.setOnLongClickListener(longListener);
        holder.settingsButton.setOnLongClickListener(longListener);

        holder.deleteLayout.setVisibility(View.GONE);

        //set data
        User user = users.get(holder.getAdapterPosition());

        if (user.getId() != null && user.getId() > 0) {  //if user is real

            //set name
            holder.name.setText(user.getName());

            //set photo
            Picasso.with(holder.mView.getContext()).load(ServiceGenerator.PROFILE_PHOTO_URL + user.getId())
                    .placeholder(holder.mView.getResources().getDrawable(R.drawable.ic_user_circle_black))
                    .error(holder.mView.getResources().getDrawable(R.drawable.ic_user_circle_black))
                    .resize(60, 60)
                    .centerCrop().transform(new CircleTransform()).into(holder.picture);

            //set proper button
            holder.settingsButton.setVisibility(View.GONE);
            holder.remember.setVisibility(View.GONE);


        } else { //if user is not real

            //set name
            if (user.getName() != null) {
                holder.name.setText(user.getName());
            } else {
                holder.name.setText(context.getString(R.string.person) + " " + (position + 1));
            }

            String photoPath = user.getPhoto();
            if (photoPath != null) {
                Picasso.with(holder.mView.getContext()).load(Uri.parse(photoPath))
                        .resize(50, 50)
                        .centerCrop().transform(new CircleTransform()).into(holder.picture);
            }
        }

    }

    // Create an instance of the dialog fragment and show it
    public void showNoticeDialog(int position) {
        if (SharedPreferencesManager.getIdUser() != 0) {
            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            AddUserDialog dialog = new AddUserDialog();
            dialog.setArguments(bundle);
            dialog.show(((AppCompatActivity) context).getSupportFragmentManager(), "dialog");
        } else {
            AssignUserExplorerDialog dialog = new AssignUserExplorerDialog();
            dialog.show(((AppCompatActivity) context).getSupportFragmentManager(), "dialog");
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return users.size();
    }

    //hide possible bins
    public boolean updateView(View v, ViewHolder holder) {

        for (int i = 0; i < getItemCount(); i++) {
            View otherView = ((RecyclerView) v.getParent()).getChildAt(i);

            if (i != holder.getLayoutPosition() && otherView != null) {
                RelativeLayout deleteLayout = (RelativeLayout) otherView.findViewById(R.id.deleteview);
                if (deleteLayout.isShown()) {
                    deleteLayout.setVisibility(View.GONE);
                    return true;
                }
            }

        }
        return false;
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one card per item, and
    // you provide access to all the views for a data item in a card holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        // each data item is just a string in this case
        public TextView name;
        public TextView remember;
        public ImageView picture;
        public ImageButton settingsButton;

        public RelativeLayout mainLayout;
        public RelativeLayout deleteLayout;
        public ImageView deleteButton;

        public ViewHolder(View v) {
            super(v);
            mView = v;
            name = (TextView) v.findViewById(R.id.username);
            remember = (TextView) v.findViewById(R.id.remember);

            picture = (ImageView) v.findViewById(R.id.user_picture);
            settingsButton = (ImageButton) v.findViewById(R.id.settings);


            mainLayout = (RelativeLayout) v.findViewById(R.id.mainview);
            //layout displayed onLongClick
            deleteLayout = (RelativeLayout) v.findViewById(R.id.deleteview);
            //button to click when background layout is displayed
            deleteButton = (ImageView) v.findViewById(R.id.delete_button);
        }



    }
}
